# Inter-hemispheric REM
=======================

Code acompanying the publication:

> Interhemispheric competition during sleep \
> Fenk, L.A., Riquelme J.L., and Laurent G. \
> Nature (2023) \
> https://doi.org/10.1038/s41586-023-05827-w


Project Organization
--------------------

    ├── LICENSE
    ├── README.md
    ├── requirements.txt
    ├── setup.py             <- Makes module installable with pip install -e .
    │    
    ├── ihrem                <- Package encapsulating all code in this project.
    │    ├── extract.py      <- Main code to process raw data.
    │    ├── io_*.py         <- Code to load data recorded from Neuronexus and Neuropixel probes.
    │    ├── stacks.py       <- Data container for tensor-like data
    │    ├── timeslice.py    <- Data container for sets of time windows
    │    ├── trains.py       <- Data container for spike trains
    │    ├── figs            <- Code to generate figures for the article (see notebooks/figs/)
    │    └── analysis
    │        ├── sne*.py     <- Code to extract Sharp Negative events and match them in bilateral pairs.
    │        ├── xcorr*.py   <- Code to extract cross-correlations from LFP.
    │        └── sleep*.py   <- Code to extract beta power and detect Sharp Wave Ripples.
    │    
    └── notebooks
         ├── figs            <- Jupyter notebooks to generate figures for the article.
         └── temp            <- Intermediate data that has been transformed through notebooks.


--------

You can install editable package as

    pip install -e .

Data is processed using `ihrem/extract.py` which orchestrates the analysis code in `ihrem/analysis`. See notebooks in `notebooks/figs` for examples of visualizing the processed results. Refer to the publication for data availability and detailed methods.