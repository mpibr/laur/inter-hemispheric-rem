from setuptools import setup, find_packages

setup(
    name='ihrem',
    version='1.0',
    description='Inter-hemispheric REM sleep',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
        ],
    }
)
