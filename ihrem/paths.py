"""
Locate files (raw data and partial results).
All information is in an excel sheet that we process here (the Registry).
All partial results and processed data are stored in HDF5 files within a folder "swsort" next to the data.
"""
import datetime
import functools
import itertools
import logging
from pathlib import Path

import numpy as np
import pandas as pd
from tqdm.auto import tqdm as pbar

from ihrem.stacks import Stack, StackSet


def get_root():
    """path to collaboration data folder"""
    root = Path('/gpfs/laur/data/fenkl/spikes')
    return root


class Registry:
    def __init__(self, df: pd.DataFrame):
        assert df.index.is_unique, df.index[df.index.duplicated()]

        valid_path = df['raw_path'].notna()
        for name in df.index[~valid_path]:
            logging.error(f'Dropping {name}: missing path.')
        df = df.loc[df['raw_path'].notna()].copy()

        assert (df.groupby('animal')['lesion'].nunique() <= 1).all()

        self.reg = df.copy()

        paths = pd.Series({name: self.get_path(name) for name in self.experiment_names})
        valid_path = paths.map(lambda p: p.exists())
        for name in self.reg.index[~valid_path]:
            logging.error(f'Dropping {name}: path does not exist ({paths[name]})')
        self.reg = self.reg.loc[df['raw_path'].notna()].copy()

    @classmethod
    def read_excel(cls, reg_path=None, sheet_name='swr'):
        """load a stored registry of all of the experiments and important paths"""

        if reg_path is None:
            reg_path = get_root() / 'registry_merged.xlsx'

        # noinspection PyArgumentList
        reg = pd.read_excel(reg_path, index_col='name', sheet_name=sheet_name)

        df = reg.dropna(how='all')

        to_ignore = df['ignore'].fillna(False).astype(bool)
        df = df[~to_ignore]

        return cls(df)

    def sel_mask(self, mask: pd.Series):
        return self.__class__(self.reg.loc[mask])

    def __getitem__(self, item) -> pd.Series:
        return self.reg.__getitem__(item)

    def sel_per_animal(self):
        """take last night per animal"""
        exp_names = self.reg.reset_index().groupby('animal')['name'].max().values
        return self.sel_mask(exp_names)

    def is_bilat(self, area: str) -> pd.Series:
        count = np.zeros(len(self.reg))
        for col in ['probe0', 'probe1', 'probe2']:
            count = count + (self[col].str.lower() == area.lower()).astype(int)

        return count >= 2

    def is_healthy(self) -> pd.Series:
        return self['lesion'].isna()

    def is_lesion(self, area: str) -> pd.Series:
        return self['lesion'].str.lower().str.contains(area.lower()).fillna(False)

    def __len__(self):
        return len(self.reg)

    def get_events(self, exp_name, event_cols=('light off', 'light on', 'sleep on', 'sleep off')) -> pd.Series:
        """get timestamps for all imporant events
        (like turning lights off) for this epxeriment """

        event_cols = list(event_cols)

        entries = self.reg.loc[exp_name, event_cols].dropna()

        return entries.map(
            lambda v: datetime.timedelta(
                hours=v.hour,
                minutes=v.minute,
                seconds=v.second,
                microseconds=v.microsecond
            ).total_seconds() * 1_000,
        )

    def get_all_events(self) -> pd.DataFrame:
        """get timestamps for all imporant events
        (like turning lights off) for all epxeriments """
        return pd.DataFrame({
            exp_name: self.get_events(exp_name)
            for exp_name in self.experiment_names
        })

    def get_path(self, experiment_name, col='raw'):
        """
        get full path to a part of the experiment

        :param experiment_name: for example GL587_20200115_sleep_g0
        :param col: one of:
            raw
            swsort
            jrclust
        """

        if col not in self.reg.columns and f'{col}_path' in self.reg.columns:
            col = f'{col}_path'

        if col not in self.reg.columns:
            raise KeyError('Col must be one of: ' + ', '.join([c for c in self.reg.columns if c.endswith('_path')]))

        path = Path(self.reg.loc[experiment_name, col])

        if not path.is_absolute():
            path = get_root() / path

        if not path.exists():
            logging.warning(f"Path doesn't exist: {path}")

        return path

    def get_path_sne(self, exp_name, probe: int, ch: int, suffix='') -> Path:
        return self.get_path(exp_name) / f'swsort/sne_p{probe}c{ch}{suffix}.h5'

    def get_path_sne_all(self, exp_name, area='CLA', suffix='') -> list:
        paths = []

        for i, (p, ch) in enumerate(self.get_probe_channels(exp_name, area=area)):
            # noinspection PyTypeChecker
            paths.append((p, ch, self.get_path_sne(exp_name, p, ch, suffix=suffix)))

        return paths

    def get_path_matching(
            self, exp_name,
            suffix='', area=''
    ) -> Path:
        pcs = self.get_probe_channels(exp_name, area=area)
        assert len(pcs) >= 2
        filename = f'sne_matching_p{pcs[0][0]}c{pcs[0][1]}_p{pcs[1][0]}c{pcs[1][1]}{suffix}.h5'
        return self.get_path(exp_name) / f'swsort/{filename}'

    def get_path_power(
            self, exp_name, band: str, probe: int, ch: int, sliding_win: int, sliding_step: int,
            suffix=''
    ) -> Path:

        location = f'p{int(probe)}c{int(ch)}'
        params = f'w{sliding_win:g}_s{sliding_step:g}'
        return self.get_path(exp_name) / f'swsort/power_{band}_{location}_{params}{suffix}.h5'

    def get_path_xcorr(
            self, exp_name, probe0: int, probe1: int, ch0: int, ch1: int, sliding_win: int,
            low_hz=40, suffix='',
    ) -> Path:
        location = f'p{int(probe0)}c{int(ch0)}_p{int(probe1)}c{int(ch1)}'
        params = f'w{sliding_win:g}'
        return self.get_path(exp_name) / f'swsort/xcorr_{location}_{params}{suffix}_{low_hz:g}hz.h5'

    def get_path_xcorr_area(
            self, exp_name, sliding_win: int,
            low_hz=40, suffix='', area=''):
        pcs = self.get_probe_channels(exp_name, area=area)

        return self.get_path_xcorr(
            exp_name,
            pcs[0][0], pcs[1][0], pcs[0][1], pcs[1][1],
            sliding_win=sliding_win, low_hz=low_hz, suffix=suffix,
        )

    def collect_paths_power(self, band, sliding_win, sliding_step, areas=None, missing=False):
        to_extract = []

        for exp_name in self.experiment_names:

            probe_idcs = self._get_valid_probe_idcs(exp_name, areas=areas)

            for idx in probe_idcs:

                ch = self.reg.loc[exp_name, f'ch{idx}']
                if np.isnan(ch):
                    logging.error(f'Missing channel {idx} for {exp_name}')
                    continue

                ch = int(ch)

                results_path = self.get_path_power(exp_name, band, idx, ch, sliding_win, sliding_step)

                if (not results_path.exists()) == missing:
                    to_extract.append((exp_name, results_path, idx, ch))

        return pd.DataFrame.from_records(
            to_extract,
            columns=['exp_name', 'path', 'probe', 'channel'],
        )

    def collect_paths_xcorr(self, sliding_win, suffix='', missing=False, areas=None, low_hz=40) -> pd.DataFrame:
        """
        :param sliding_win:
        :param suffix:
        :param missing:
        :param areas: List of valid areas to compute x-corr. If none specified, any area is valid.
        :return:
        """
        to_extract = []

        for exp_name in self.experiment_names:

            probe_idcs = self._get_valid_probe_idcs(exp_name, areas=areas)

            import itertools
            for a, b in itertools.combinations(probe_idcs, 2):

                ch_a = self.reg.loc[exp_name, f'ch{a}']
                ch_b = self.reg.loc[exp_name, f'ch{b}']

                if not np.isnan(ch_a) and not np.isnan(ch_b):
                    ch_a = int(ch_a)
                    ch_b = int(ch_b)

                    results_path = self.get_path_xcorr(
                        exp_name, a, b, ch_a, ch_b, sliding_win,
                        suffix=suffix, low_hz=low_hz)

                    if (not results_path.exists()) == missing:
                        to_extract.append((exp_name, results_path, a, b, ch_a, ch_b))

        return pd.DataFrame.from_records(
            to_extract,
            columns=['exp_name', 'path', 'p0', 'ch0', 'p1', 'ch1'],
        )

    def collect_paths_sne(self, missing=False, areas=None, suffix='') -> pd.DataFrame:
        to_extract = []

        for exp_name in self.experiment_names:

            probe_idcs = self._get_valid_probe_idcs(exp_name, areas=areas)

            for idx in probe_idcs:

                if self.loc[exp_name, f'probe{idx}'] in ['CLA', 'BST']:

                    ch = int(self.reg.loc[exp_name, f'ch{idx}'])

                    results_path = self.get_path_sne(exp_name, idx, ch, suffix=suffix)

                    if (not results_path.exists()) == missing:
                        to_extract.append((exp_name, results_path, idx, ch))

        return pd.DataFrame.from_records(
            to_extract,
            columns=['exp_name', 'path', 'probe', 'channel'],
        )

    def collect_paths_matching(self, area='', missing=False, suffix='') -> pd.DataFrame:

        paths = []

        for exp_name in self.experiment_names:
            results_path = self.get_path_matching(exp_name, area=area, suffix=suffix)

            if (not results_path.exists()) == missing:
                paths.append((exp_name, results_path))

        return pd.DataFrame.from_records(
            paths,
            columns=['exp_name', 'path']
        )

    def _get_valid_probe_idcs(self, exp_name, areas=None):
        probes = self.reg.loc[exp_name, ['probe0', 'probe1', 'probe2']].dropna()

        if areas is not None:
            probes = probes[probes.isin(areas)]

        probe_idcs = [int(p[-1]) for p in probes.index]

        return probe_idcs

    # noinspection PyTypeChecker
    @property
    @functools.wraps(pd.DataFrame.loc)
    def loc(self):
        return self.reg.loc

    @property
    def experiment_names(self):
        return self.reg.index

    def _repr_html_(self):
        """pretty print on notebooks"""
        # noinspection PyProtectedMember
        return self.reg._repr_html_()

    def group_exps(self, names=None, by=('state', 'lesion'), count_label=True) -> (pd.Series, pd.DataFrame):
        """
        Groups experiments by multiple columns and creates a unique style for each group.
        Style includes "color" and "label".
        """
        if names is None:
            names = self.experiment_names

        if isinstance(by, str):
            by = [by]

        by = list(by)

        entries = self.reg.loc[names].fillna('none').copy()
        # allow grouping by the name
        entries = entries[pd.Index(by).difference(['name'])]

        group_sizes = entries.reset_index(drop=False).groupby(by).size().sort_values(ascending=False)
        groups_ids = pd.Series(np.arange(len(group_sizes)), group_sizes.index, name='group_id')
        groups: pd.DataFrame = groups_ids.reset_index().set_index('group_id')

        labels = {}

        for i, vs in groups[by].T.items():
            labels[i] = ', '.join([str(v) for v in vs if v != 'none'])
            if labels[i] == '':
                labels[i] = 'none'

            if count_label:
                labels[i] = f'{labels[i]} ({group_sizes[i]})'

        groups['label'] = pd.Series(labels)
        groups['color'] = ['xkcd:grey'] + [f'C{i}' for i in np.arange(len(groups) - 1)]

        entry_group = pd.merge(entries.reset_index(drop=False),
                               groups.reset_index(), how='left').set_index('name')['group_id']

        return entry_group, groups

    def iter_exp_groups(self, **kwargs):
        """
        Iterate experiments in groups by some criteria.

        Use like:

        f, ax = plt.subplots(constrained_layout=True, figsize=(4, 1.5))

        groups = reg.iter_exp_groups(names=bilat_exps, by=['lesion'])
        handles = {}

        for label, names, desc in groups:
            for exp_name in names:

                handles[desc['label']] = ax.plot(
                    hists.index.mid,
                    hists[exp_name],
                    color=desc['color'],
                )[0]

        ax.legend(
            list(handles.values()),
            list(handles.keys()),
            loc='upper right',
            fontsize=6,
        )


        :param kwargs: parameters to group_exps
        :return:
        """

        entry_groups, groups = self.group_exps(**kwargs)

        entry_groups = entry_groups.groupby(entry_groups).groups

        for label, names in entry_groups.items():
            yield label, names, groups.loc[label]

    def get_exp_short_desc(self, exp_name, cols=('probe type', 'lesion', 'stim', 'state')):
        """get a short string description of this experiment"""
        return ', '.join(self.reg.loc[exp_name, list(cols)].dropna())

    def get_loader(self, exp_name, accept_non_interp=False):
        """
        Get a loader of raw data. Probe-dependent.
        """

        probe = self.reg.loc[exp_name, 'probe type']

        if probe == 'neuropixel':
            from ihrem import io_neuropixel
            try:
                raw = io_neuropixel.MultiProbeLoader.multiprobe_interp(self.get_path(exp_name, 'raw'))
            except FileNotFoundError:
                if not accept_non_interp:
                    raise
                else:
                    raw = io_neuropixel.MultiProbeLoader.multiprobe_spikeglx(self.get_path(exp_name, 'raw'))

        else:
            assert probe in ['neuronexus', 'CamNeurotech'], f'Unknown probe {probe}'

            from ihrem import io_neuralynx
            raw = io_neuralynx.MultiNCSLoader.from_folder(self.get_path(exp_name, 'raw'))

        return raw

    def get_probe_channels(self, exp_name, area='') -> list:

        mask = self.loc[exp_name, ['probe0', 'probe1', 'probe2']].dropna().str.contains(area)
        mask = mask.values

        return [
            (int(name[-1]), int(idx))
            for name, idx in self.reg.loc[exp_name, ['ch0', 'ch1', 'ch2']].dropna()[mask].items()
        ]

    def load_all_sne(self, exp_name, **kwargs) -> pd.DataFrame:

        paths = self.get_path_sne_all(exp_name, **kwargs)

        all_sns = []

        for i, (p, ch, path) in enumerate(paths):
            # noinspection PyTypeChecker
            df: pd.DataFrame = pd.read_hdf(path)

            df['probe'] = p
            df['channel'] = ch

            all_sns.append(df)

        return pd.concat(all_sns, axis=0, ignore_index=True)

    def load_all_beta(self, exp_name, band='beta', sliding_win=10_000, sliding_step=1_000, area='') -> pd.DataFrame:
        df = pd.DataFrame({
            (p, c): pd.read_hdf(
                self.get_path_power(
                    exp_name, band, p, c,
                    sliding_win=sliding_win, sliding_step=sliding_step)
            )
            for p, c in self.get_probe_channels(exp_name, area=area)
        })

        assert len(df) > 0

        df.rename_axis(columns=['probe', 'channel'], inplace=True)

        return df

    def load_all_beta_norm(self, exp_name, area='CLA', exp_valid_win=None, add_max=True):
        all_beta = self.load_all_beta(exp_name, band='beta', area=area)

        # note it is important to crop before computing quantiles
        # because motion artifacts are much more likely at the beginning/end of the recording
        if exp_valid_win is not None:
            all_beta = exp_valid_win.crop_df(all_beta)

        all_beta = all_beta / all_beta.quantile(.999)

        all_beta = all_beta.droplevel('channel', axis=1)
        all_beta.columns = [f'ch{col}' for col in all_beta.columns]

        if add_max:
            all_beta['beta_max'] = all_beta.max(axis=1)

        return all_beta

    def load_exp_xcorr_combs(self, exp_names, which=None, **xcorr_kwargs) -> StackSet:

        if isinstance(which, str):
            which = [which]

        xcorr_triplet = {}
        all_paths = {}

        for exp_name in exp_names:
            probe_channels = self.get_probe_channels(exp_name)

            for (p0, c0), (p1, c1) in itertools.combinations(probe_channels, 2):

                area0 = self.loc[exp_name, f'probe{p0}']
                area1 = self.loc[exp_name, f'probe{p1}']

                side0 = self.loc[exp_name, f'side{p0}']
                side1 = self.loc[exp_name, f'side{p1}']

                swap = False

                # give preference to claustrum as our reference
                # this means we may need to swap the lags
                if area1 == 'CLA' and area0 != 'CLA':
                    swap = True
                    area0, area1 = area1, area0
                    side0, side1 = side1, side0

                if area0 == area1:
                    key = f'{area0} bilat'

                elif side0 == side1:
                    key = f'{area0}-{area1} ipsi'

                else:
                    key = f'{area0}-{area1} contra'

                if which is None or key in which:
                    path = self.get_path_xcorr(exp_name, p0, p1, c0, c1, **xcorr_kwargs)

                    if path.exists():
                        all_paths[key, exp_name] = path, swap

                    else:
                        logging.warning(f'{exp_name}: Missing expected x-corr {p0}-{c0} vs {p1}-{c1}: {path}')

        for (key, exp_name), (path, swap) in pbar(all_paths.items(), desc='load x-corr'):
            print(f'loading {path}')
            xcorr = Stack.load_hdf(str(path), 'xcorr')

            if swap:
                print('swapping')
                xcorr = xcorr.replace_dim('lag', 'lag', xcorr.coords['lag'] * -1)
                xcorr = xcorr.sortby('lag')

            xcorr_triplet[key, exp_name] = xcorr

        return StackSet.from_dict(xcorr_triplet, names=['pair', 'exp'])
