"""
General utility code to plot traces and spike trains
"""
import itertools
import typing

import matplotlib.cm
import matplotlib.colors
import matplotlib.patches
import matplotlib.ticker
import matplotlib.transforms
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from tqdm.auto import tqdm

from ihrem import timeslice
from ihrem.stacks import Stack
from ihrem.timeslice import ms

COLORS_CHANNEL = dict(
    ch0='#0053A3',  # right, blue
    ch1='#FF0000',  # left, red
    ch2='#44AF69',  # green
    ch3='#F5C000',  # yellow
)

COLORS = {
    'lead_ch1': COLORS_CHANNEL['ch1'],
    'lead_ch0': COLORS_CHANNEL['ch0'],
    'is_lead_ch1': COLORS_CHANNEL['ch1'],
    'is_lead_ch0': COLORS_CHANNEL['ch0'],
    'beta_ch1': COLORS_CHANNEL['ch1'],
    'beta_ch0': COLORS_CHANNEL['ch0'],
    'beta_max': '#663C00',
    'lead_diff': '#AE76A6',
    'lead_score': '#AE76A6',

    'ch0': COLORS_CHANNEL['ch0'],
    'ch0_light': '#70BAFF',
    'ch0_dark': '#003566',
    'ch1': COLORS_CHANNEL['ch1'],
    'ch1_light': '#FFADAD',
    'ch1_dark': '#8F0000',
    'ch2': COLORS_CHANNEL['ch2'],  # green
    'ch2_light': '#98D7AE',
    'ch2_dark': '#225835',
    'ch3': COLORS_CHANNEL['ch3'],  # yellow
    'ch3_light': '#FFD747',
    'ch3_dark': '#7A6000',
    'none': 'xkcd:charcoal',
    'syn': 'xkcd:charcoal',
    'sws': 'xkcd:silver',
    'rem': '#FF9F1C',

    'x': 'xkcd:marigold',

    'light off': 'k',
    'light on': 'k',
    'sleep on': 'xkcd:magenta',
    'sleep off': 'xkcd:magenta',

    'ch1_to_sws': 'xkcd:grey blue',
    'ch0_to_sws': 'xkcd:tan',
    'sws_to_ch1': COLORS_CHANNEL['ch1'],
    'sws_to_ch0': COLORS_CHANNEL['ch0'],
    'ch0_to_ch1': 'xkcd:burnt orange',
    'ch1_to_ch0': 'xkcd:royal blue',
}


def _make_ch_cmaps(seq=('_light', '', '_dark')) -> dict:
    return {
        ch: matplotlib.colors.LinearSegmentedColormap.from_list(
            ch,
            [
                COLORS[f'{ch}{shade}'] if f'{ch}{shade}' in COLORS else shade
                for shade in seq
            ],
            N=256
        )
        for ch in ['ch0', 'ch1', 'ch2', 'ch3']
    }


CMAPS_CHANNEL = _make_ch_cmaps(['_light', '', '_dark'])
CMAPS_CHANNEL_WHITE = _make_ch_cmaps(['w', '', '_dark'])


###########################################################################################
# Plotting across mutiple time scales


class MultiUnitFormatter(matplotlib.ticker.Formatter):
    """
    Use like:

        ax.xaxis.set_major_formatter(plot.MultiUnitFormatter.for_time())

    Labels become wordy because of embedded unit. You can keep the number of ticks under control with:

        ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(nbins=3))

    """

    def __init__(self, scales, thresh, units, show_unit=True, as_offset=False):
        """
        :param scales: a list of scales that transition between units
        :param thresh: a list of thresholds to indicate when change units. Expressed, pairwise, in the lower units
        :param as_offset: plot the distance relative to xlim
        :param units: the strings of the units
        """
        self.scales = scales
        self.thresh = thresh
        self.units = units
        self.as_offset = as_offset
        self.show_unit = show_unit

    @classmethod
    def for_time(cls, **kwargs):
        return cls(
            *zip(*[
                [1., 1., 'ms'],  # ms
                [1000., 5000., 'sec'],  # ms -> sec
                [60., 120., 'min'],  # sec -> min
                [60., 120., 'hours'],  # min -> hours
                [24., 72., 'days'],  # hours -> days
            ]),
            **kwargs
        )

    def __call__(self, x, pos=None):
        """
        """
        vmin, vmax = self.axis.get_view_interval()

        if self.as_offset:
            x -= vmin
            vmax -= vmin
            vmin = 0

        span = vmax - vmin

        i = 0
        while i < len(self.thresh) - 1 and span > self.thresh[i + 1]:
            span = span / self.scales[i + 1]
            x = x / self.scales[i + 1]
            i += 1

        t = f'{x:.2f}'.rstrip('0').rstrip('.')
        return f'{t} {self.units[i]}'


def set_time_axis(ax, which='x', major=None, minor=None, as_offset=False, tight=True, label=None, show_unit=True):
    """set axis to dynamically switch between temporal timescales (ms, s, min, hours)"""
    assert which in ('x', 'y')
    axis = ax.yaxis if which == 'y' else ax.xaxis

    if label is not None:
        axis.set_label_text(label)

    if tight is not None:
        ax.autoscale(enable=True, axis=which, tight=tight)

    formatter = MultiUnitFormatter.for_time(show_unit=show_unit, as_offset=as_offset)
    axis.set_major_formatter(formatter)

    set_time_ticks(
        ax,
        which=which,
        major=major,
        minor=minor,
    )


def set_time_ticks(
        ax,
        major=None,
        minor=None,
        which='x',
        minor_length=2,
        major_length=3,
        scale=None,
        label=None,
        tight=True,
):
    """set major xticks to mark every hour and minor every 10 minutes"""
    assert which in ('x', 'y')
    axis = ax.yaxis if which == 'y' else ax.xaxis

    ax.tick_params(which='minor', length=minor_length)
    ax.tick_params(which='major', length=major_length)

    auto_major, auto_minor = _auto_select_tick_steps(ax, which=which)

    if minor is None:
        if major is not None:
            minor = major / 2
        else:
            minor = auto_minor

    if major is None:
        major = auto_major

    axis.set_major_locator(matplotlib.ticker.MultipleLocator(timeslice.to_ms(major)))
    axis.set_minor_locator(matplotlib.ticker.MultipleLocator(timeslice.to_ms(minor))),

    if scale is not None:
        if isinstance(scale, str):
            scale = ms(**{scale: 1})

        def scale_ticks(x, _):
            return f'{x / scale:g}'

        axis.set_major_formatter(matplotlib.ticker.FuncFormatter(scale_ticks))

    if label is not None:
        axis.set_label_text(label)

    if tight is not None:
        ax.autoscale(enable=True, axis=which, tight=tight)


def _auto_select_tick_steps(ax, which='x') -> tuple:
    """
    Select the steps of minor and major ticks according
    to the ax current data limits, so that they match common time units.

    :param ax:
    :param which:
    :return:
    """
    lim = ax.get_xlim() if which == 'x' else ax.get_ylim()

    duration = max(lim) - min(lim)

    sections = {
        ms(milliseconds=100): (ms(milliseconds=10), ms(milliseconds=5)),
        ms(seconds=1): (ms(milliseconds=100), ms(milliseconds=50)),
        ms(seconds=10): (ms(seconds=2.5), ms(milliseconds=100)),
        ms(minutes=1): (ms(seconds=10), ms(seconds=5)),
        ms(minutes=2): (ms(seconds=30), ms(seconds=10)),
        ms(minutes=5): (ms(minutes=1), ms(seconds=20)),
        ms(minutes=10): (ms(minutes=1), ms(minutes=.5)),
        ms(hours=1): (ms(minutes=10), ms(minutes=5)),
        ms(hours=5): (ms(minutes=30), ms(minutes=10)),
        ms(hours=10): (ms(hours=2), ms(hours=1)),
        np.inf: (ms(hours=5), ms(hours=2.5)),
    }

    assert duration < max(sections.keys())

    for thresh, steps in sections.items():
        if duration <= thresh:
            return steps


def set_ticks_named_time(ax, which='x', lim=None):
    """
    set major xticks to mark human-friendly durations
    particularly helpful for log-plots
    """
    assert which in ('x', 'y')
    axis = ax.yaxis if which == 'y' else ax.xaxis

    if lim is None:
        lim = ax.get_ylim() if which == 'y' else ax.get_xlim()

    ax.tick_params(which='minor', length=2)
    ax.tick_params(which='major', length=3)

    named_times = {
        '1s': timeslice.ms(seconds=1),
        '10s': timeslice.ms(seconds=10),
        # '30s': timeslice.ms(seconds=30),
        '1min': timeslice.ms(minutes=1),
        # '2min': timeslice.ms(minutes=2),
        # '5min': timeslice.ms(minutes=5),
        '10min': timeslice.ms(minutes=10),
        # '30min': timeslice.ms(minutes=30),
        '1h': timeslice.ms(hours=1),
    }

    named_times = {
        name: t
        for name, t in named_times.items()
        if lim[0] <= t <= lim[1]
    }
    axis.set_ticks(list(named_times.values()))
    axis.set_ticklabels(list(named_times.keys()))


###########################################################################################
# Plotting multiple traces


def get_default_mappable(
        c: np.ndarray,
        norm: typing.Optional[matplotlib.colors.Normalize],
        cmap: typing.Optional[matplotlib.colors.Colormap]):
    """Just fill in a mappable with sensible defaults and
    build an object that can be displayed in a colorbar"""

    if norm is None:
        norm = matplotlib.colors.Normalize(vmax=np.max(c), vmin=np.min(c))
        if len(c) == 1:
            # make sure norm returns .5
            norm = matplotlib.colors.Normalize(vmax=c[0] + .5, vmin=c[0] - .5)

    if cmap is None:
        cmap = 'viridis'

    if isinstance(cmap, str):
        cmap = matplotlib.cm.get_cmap(cmap)

    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array(c)
    return sm


def plot_traces(
        ax, lines, x=None, c=None, cmap='viridis', norm=None, pbar=None, sort_ascending=None,
        orientation='horizontal',
        max_sample=None,
        labels=None,
        **kwargs
):
    """
    lines is expected to contain the lines as COLUMNS
    lines will be plotted in the order specified by c
    By default, c is linspace(0, 1) following the order of the columns in lines

    :param max_sample: int. Indicate how many lines to show at maximum. They will be selected randomly.
        If not provided, all lines are plot (default).

    :param ax:
    :param lines:
    :param x:
    :param c:
    :param cmap:
    :param norm:
    :param pbar:
    :param sort_ascending:
    :param orientation:
    :param labels:
    :param kwargs:
    :return:
    """
    assert orientation in ('horizontal', 'vertical')

    if hasattr(lines, 'ndim'):
        assert lines.ndim == 2, lines.shape

        if 'time' in lines.dims:
            lines = lines.transpose('time', ...)

        if x is None:
            x = lines.coords[lines.dims[0]]

        if c is None:
            c = lines.coords[lines.dims[1]]

        lines = lines.values.T

    if isinstance(lines, np.ndarray):
        lines = pd.DataFrame(
            lines.T,
            index=x.index if isinstance(x, pd.Series) else None,
            columns=c.index if isinstance(c, pd.Series) else None,
        )

    lines: pd.DataFrame

    if c is None:
        cols = lines.columns
        if isinstance(cols, pd.IntervalIndex):
            cols = cols.mid

        if np.issubdtype(cols.dtype, np.number):
            c = cols

        else:
            c = np.linspace(0, 1, lines.shape[1])

    if not isinstance(c, pd.Series):
        c = pd.Series(np.asarray(c), index=lines.columns)

    if labels is not None:
        if isinstance(labels, (np.ndarray, list, tuple, pd.Index)):
            labels = pd.Series(labels, index=lines.columns)

    c = c.loc[c.index.intersection(lines.columns)]

    if x is None and isinstance(lines, pd.DataFrame):
        x = lines.index

    if isinstance(x, pd.IntervalIndex):
        x = x.mid

    if isinstance(x, pd.TimedeltaIndex):
        x = x.values

    cmap = matplotlib.cm.get_cmap(cmap)

    if norm is None:
        # cv = c.values
        # diff = np.diff(cv) * .5
        # boundaries = np.concatenate([[cv[0]], cv]) + np.concatenate([[-diff[0]], diff, [diff[-1]]])
        # norm = matplotlib.colors.BoundaryNorm(boundaries, len(cv))
        vrange = c.min(), c.max()
        # if we have a single value, make sure matplotlib plots the MIDDLE color of the cmap
        # instead of the lowest one
        if np.isclose(vrange[1] - vrange[0], 0):
            vrange = vrange[0] - vrange[0] * .001, vrange[1] + vrange[1] * .001
        norm = matplotlib.colors.Normalize(*vrange)

    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array(c.values)

    if sort_ascending is not None:
        c = c.sort_values(ascending=sort_ascending)
        lines = lines[c.index]

    if max_sample is not None:
        c = c.sample(min(max_sample, len(c)), replace=False)

    label = kwargs.pop('label', None)

    c_items = c.items()
    if pbar is not None:
        c_items = pbar(c_items, total=len(c), desc='traces')

    for i, (col, trace_c) in enumerate(c_items):

        if labels is not None:
            label = labels.loc[col]

        if orientation == 'horizontal':
            ax.plot(x, lines[col].values, color=sm.cmap(sm.norm(trace_c)), label=label, **kwargs)
        else:
            ax.plot(lines[col].values, x, color=sm.cmap(sm.norm(trace_c)), label=label, **kwargs)

    return sm


def plot_traces_solid(
        ax, lines,
        linewidth=.5,
        show_legend=False,
        xcol='time',
        **kwargs,
):
    """plot traces using the default sequence of colors instead of a color map"""
    ycol = lines.get_coords_names_except(xcol)[0]

    for i, y_value in enumerate(lines.coords[ycol]):
        ax.plot(
            lines.coords[xcol],
            lines.sel(**{ycol: y_value}).values,
            label=f'{ycol}={y_value}',
            linewidth=linewidth,
            **kwargs,
        )
    if show_legend:
        ax.legend()


def plot_traces_channels(
        data,
        zscore=True,
        sharey='all',
        linewidth=.5,
        major=timeslice.ms(minutes=2),
        minor=timeslice.ms(minutes=.5),
        axs=None,
):
    """
    Plot each trace, corresponding to different channels,
    in a different axis as rows.

    :param data:
    :param zscore:
    :param sharey:
    :param linewidth:
    :param major:
    :param minor:
    :param axs:
    :return:
    """
    if zscore:
        data = data.zscore('time')

    chan_count = len(data.coords['channel'])

    if axs is None:
        f, axs = plt.subplots(
            constrained_layout=True, sharex='all', sharey=sharey, nrows=chan_count,
            squeeze=False, figsize=(7, 2))
        axs = axs[:, 0]

    for i, (ch, ch_data) in enumerate(data.iter_dim('channel')):
        ax = axs[i]

        for _, trace in ch_data.iter_except('time'):
            ax.plot(
                trace.coords['time'],
                trace.values,
                color=COLORS[f'ch{ch}'],
                linewidth=linewidth
            )

        ax.autoscale(enable=True, axis='y', tight=True)

        if i < chan_count - 1:
            ax.spines['bottom'].set_visible(False)
            ax.tick_params(bottom=False, which='both')

        if zscore:
            ax.set(ylabel='z-score')

        else:
            ax.set(ylabel='uV')

    ax = axs[-1]
    set_time_axis(ax, as_offset=True, major=major, minor=minor)

    return axs


def drop_spine(ax, which: str):
    """
    Hide a spine and its ticks:
        drop_spine(ax, 'bottom')

    which can be 'all'
    """
    if which == 'all':
        for which in ['bottom', 'left', 'right', 'top']:
            drop_spine(ax, which)

    if which == 'x':
        which = 'bottom'

    if which == 'y':
        which = 'left'

    ax.spines[which].set_visible(False)
    ax.tick_params(**{which: False, f'label{which}': False}, which='both')


def plot_traces_reduced(
        stack: Stack, dim, c=None, norm=None, cmap=None, ax=None,
        reduce=np.mean, shade=np.std,
        xcol='time',
        line_zorder=10, shade_zorder=5, shade_alpha=.25, linewidth=.5, **kwargs):
    """
    Plot a rank 3 stack after reducing one of its dimensions (eg taking the mean).
    Optionally plot a shade for some metric of the reduction (eg taking the std)

    Example:

        plot_traces_reduced(all_waveforms, 'peak', shade=None, c=np.min)

    :param stack: rank 3 tensor with a time dimension
    :param dim: dimension to reduce
    :param c: None, callable, array
    Values to use to collor each of the resulting traces. Must match in number the third dimension
    (not time or the reduced one).
    If None, the values from the index will be used.
    If a callable, the stack will be reduced on (time, dim) to yield a vector of the correct shape.
    :param norm:
    :param cmap:
    :param ax:
    :param reduce: callable to reduce (default np.mean)
    :param shade: callable to use for shading. If None, no shading.
    :param line_zorder:
    :param shade_zorder:
    :param shade_alpha:
    :param linewidth:
    :param kwargs: args to the line plotting call.
    :return:
    """
    assert xcol in stack.dims
    assert stack.ndim == 3

    if ax is None:
        f, ax = plt.subplots(constrained_layout=True)

    reduce: Stack = stack.reduce(reduce, dim)

    if shade is not None:
        shade: Stack = stack.reduce(shade, dim)
    other_dim = list(set(stack.dims) - {dim, xcol})[0]

    if c is None:
        # take the values of the remaining dimension that isn't time
        c = stack.coords[other_dim].values

    if callable(c):
        c = stack.reduce(c, [dim, xcol]).values

    sm = get_default_mappable(c, norm, cmap)

    if reduce is not None:
        plot_traces(
            ax,
            reduce,
            x=reduce.coords[xcol],
            c=c,
            cmap=sm.cmap,
            norm=sm.norm,
            linewidth=linewidth,
            zorder=line_zorder,
            **kwargs,
        )

    if shade is not None:
        for i, v in enumerate(c):
            lower = reduce - shade
            upper = reduce + shade
            ax.fill_between(
                shade.coords[xcol],
                lower.isel(**{other_dim: i}).values,
                upper.isel(**{other_dim: i}).values,
                edgecolor=None,
                facecolor=sm.cmap(sm.norm(v)),
                alpha=shade_alpha,
                zorder=shade_zorder,
            )

    set_time_axis(ax)
    ax.set(ylabel=r'$\mu V$')


def windows_shade(
        ax, windows: timeslice.Windows, ymin=0, ymax=1, by='cat',
        window_colors=None, show_excluded=False,
        show_edges=False,
        transform=None,
        **kwargs):
    """

    plot multiple windows as shaded fill_between
    :param show_excluded: bool. whether to automatically calculate and highlight any period not covered in the windows

    :param ax:
    :param windows:
    :param ymin:
    :param ymax:
    :param by:
    :param window_colors:
    :param kwargs:
    :return:
    """

    if hasattr(windows, 'wins'):
        windows = windows.wins

    if transform is None:
        transform = ax.get_xaxis_transform()

    assert isinstance(windows, pd.DataFrame)

    if by not in windows.columns:
        windows = windows.copy()
        windows[by] = 'baseline'

    if show_excluded:
        excluded = windows.invert_windows(start=ax.get_xlim()[0], stop=ax.get_xlim()[1])
        excluded['cat'] = 'excluded'
        windows = pd.concat([windows, excluded], axis=0, sort=True, ignore_index=True)

    default_kwargs = dict(
        edgecolor='none',
        linewidth=0.,
        alpha=.5,
    )
    default_kwargs.update(kwargs)
    kwargs = default_kwargs

    window_colors = get_colors_with_defaults(window_colors, windows['cat'])

    for cat, wins in windows.groupby(by):
        label = cat

        for _, (start, stop) in wins[['start', 'stop']].astype(np.float).iterrows():
            ax.fill_between(
                [start, stop],
                [ymin, ymin],
                [ymax, ymax],
                transform=transform,
                facecolor=window_colors[cat],
                label=label,
                **kwargs,
            )
            label = None

    if show_edges:
        edges = timeslice.Windows(windows).get_edges()
        for t in edges:
            ax.axvline(t, ymin=ymin, ymax=ymax, linewidth=.5, color='xkcd:black')


def plot_windows_line(ax, wins, yval, by='cat', colors=None, solid_capstyle='butt', **kwargs):
    colors = get_colors_with_defaults(colors, wins[by])

    for cat in wins.wins[by].unique():
        sel_wins = wins.sel(cat=cat)

        ax.plot(
            sel_wins.wins[['start', 'stop']].T.values,
            [yval, yval],
            color=colors[cat],
            solid_capstyle=solid_capstyle,
            **kwargs,
        )


def plot_stack_2d(
        ax, s: Stack,
        norm=None,
        cmap='RdGy_r',
        norm_iqr=None,
        norm_zero_centered=True,
        xcol='time',
):
    """
    Plot a stack as a 2d image: time vs channel (or some other dimension like depth)

    :param ax:
    :param s:
    :param cmap:

    :param norm_iqr: inter-quantile-range of values to use for color-mapping.
    Values outside this range will be saturated and undistinguishable.
    This effectively implements contrast-stretch (https://homepages.inf.ed.ac.uk/rbf/HIPR2/stretch.htm)
    which typically enhances overall contrast of an image by avoiding extreme outliers.

    Note that if the image is zero_centered, only one of the two clipping values may be applicable
    (the highest one).

    Mutually exclusive to norm

    :param norm_zero_centered:
    :param xcol:
    :return:
    """
    assert s.ndim == 2
    assert xcol in s.dims

    ycol = s.get_coords_names_except(xcol)[0]

    if norm_iqr is None and norm is None:
        norm_iqr = (0., 1.)

    assert (norm is None) == (norm_iqr is not None)

    if norm_iqr is not None:
        vrange = (
            np.quantile(s.values, norm_iqr[0]),
            np.quantile(s.values, norm_iqr[1])
        )

        if norm_zero_centered:
            vmax = np.max(np.abs(vrange))
            norm = matplotlib.colors.Normalize(-vmax, +vmax)

        else:
            norm = matplotlib.colors.Normalize(*vrange)

    s = s.transpose(..., xcol)

    # we the coordinates centered on each pixel!
    # add half a bin before/after
    extent = (
        s.coords[xcol][0] * 1.5 - s.coords[xcol][1] * .5,
        s.coords[xcol][-1] * 1.5 - s.coords[xcol][-2] * .5,

        s.coords[ycol][0] * 1.5 - s.coords[ycol][1] * .5,
        s.coords[ycol][-1] * 1.5 - s.coords[ycol][-2] * .5,
    )

    im = ax.imshow(
        s.values,
        origin='lower',
        extent=extent,
        norm=norm,
        cmap=cmap,
        aspect='auto',
    )

    _set_axis_label(ax, xcol, 'x')
    _set_axis_label(ax, ycol, 'y')

    return im


def _set_axis_label(ax, label, which):
    """
    Smart set the label of the axis with special cases for time.

    :param ax:
    :param label:
    :param which:
    :return:
    """
    axis = ax.yaxis if which == 'y' else ax.xaxis
    axis.set_label_text(label)


def plot_lfp_detailed(traces, show_xcorr=False, xcol='time'):
    """
    plot detailed view of lfp traces
    """

    ###################################
    # setup axes
    f, axs = plt.subplots(
        nrows=2, ncols=2, sharex='none', sharey='row', constrained_layout=True, figsize=(7, 4),
        gridspec_kw=dict(width_ratios=[5, 1])
    )

    ax = axs[0, 0]
    ax.get_shared_x_axes().join(*axs[:, 0])
    ax.ticklabel_format(style='plain', useOffset=False)
    ax.tick_params(labelbottom=False)

    ###################################
    # plot traces as usual
    ax = axs[0, 0]
    plot_traces(ax, traces, linewidth=.25)
    ax.set(ylabel=r'$\mu V$')

    ###################################
    # plot traces as 2d image
    ax = axs[1, 0]
    plot_stack_2d(ax, traces)

    ###################################
    # plot histogram of traces
    ax = axs[0, 1]

    bins = np.linspace(*ax.get_ylim(), 101)
    bin_centers = (bins[:-1] + bins[1:]) * .5

    for tr in tqdm(traces.transpose(..., xcol).values, desc='hists'):
        h, _ = np.histogram(tr, bins=bins)
        line = ax.plot(h, bin_centers)

        ax.scatter(
            [.9],
            [np.mean(tr)],
            marker='<',
            transform=ax.get_yaxis_transform(),
            facecolor=line[0].get_color(),
            edgecolor='none',
            s=5,
            zorder=100000,
        )

    ###########################################
    # plot xcorr of traces (this is slow)
    ax = axs[1, 1]
    if show_xcorr:
        other = traces.get_coords_names_except(xcol)[0]

        mat = traces.transpose(xcol, ...).data.to_pandas()
        corr_mat = mat.corr()

        ax.imshow(
            corr_mat.values,
            origin='lower',
            extent=(
                    traces.get_rel_win(other) + traces.get_rel_win(other)
            ),
            aspect='auto',
        )

    else:
        ax.axis('off')

    return axs


def make_axs_long_experiment(
        win_ms,
        tbin_width=timeslice.ms(hours=2),
        sharey='all',
        constrained_layout=True,
        figsize=None,
        major=timeslice.ms(minutes=10),
        minor=timeslice.ms(minutes=1),
        show_timestamp=True,
) -> dict:
    """
    Prepare axes to plot a ful experiment chopped up in sequential chunks as rows.
    Example:

        def plot_long(long_trace: pd.Series):
            tbins, axs = splot.make_axs_long_experiment(
                total_duration=raw.meta['duration_ms'],
            )

            for (start, stop), ax in zip(tbins, axs):
                section = long_trace.loc[tbin.to_slice_ms()]
                ax.plot(section.index - tbin.start, section.values, linewidth=.5)

    :return:
    """
    win_ms = timeslice.Win(*win_ms)
    tbin_width = timeslice.to_ms(tbin_width)
    t_edges = np.arange(win_ms.start, win_ms.stop, tbin_width)
    t_edges = np.append(t_edges, win_ms.stop)

    nrows = len(t_edges) - 1

    if figsize is None:
        figsize = (7, 5 / 9 * (nrows + 1))

    f, axs = plt.subplots(
        nrows=nrows,
        squeeze=False,
        sharex='all', sharey=sharey,
        constrained_layout=constrained_layout,
        figsize=figsize,
    )

    for i, tbin in enumerate(zip(t_edges[:-1], t_edges[1:])):

        tbin = slice(tbin[0], tbin[1])

        ax = axs.ravel()[i]

        ax.spines['bottom'].set_visible(False)
        ax.tick_params(bottom=False, which='both')

        timestamp = f'{timeslice.strf_ms(tbin.start, plus_sign=False)}-{timeslice.strf_ms(tbin.stop, plus_sign=False)}'
        if show_timestamp:
            ax.text(
                0, 1, timestamp,
                va='bottom', ha='left', fontsize=6, zorder=1e6, transform=ax.transAxes, clip_on=False)

    ax = axs.ravel()[-1]
    set_time_axis(ax, major=major, minor=minor)
    ax.tick_params(bottom=True, which='major', length=3)
    ax.tick_params(bottom=True, which='minor', length=2)

    tbins = [timeslice.Win(start, stop) for start, stop in zip(t_edges[:-1], t_edges[1:])]
    return dict(zip(tbins, axs.ravel()))


def plot_events_vline(ax, events: pd.Series, **kwargs):
    """plot a vertical line for every event"""
    for name, e in events.items():
        ax.axvline(e, color=COLORS[name], zorder=1e4, linewidth=1, clip_on=False, **kwargs)


def plot_traces_wrapped(
        all_traces=None, events=None, wins=None, axhline=None,
        linewidth=.5, alpha=.9,
        figsize=None,
        tbin_width=timeslice.ms(hours=2.5),
        suptitle=None,
        ylim=(-.25, 1),
        major=None, minor=None,
        colors=None,
        show_timestamp=True,
):
    """plot multiple traces, events and shaded windows for long experiments as wrapped axes"""

    if colors is None:
        colors = COLORS
    else:
        colors = {**colors, **COLORS}

    axs = make_axs_long_experiment(
        (all_traces.index.min(), all_traces.index.max()),
        tbin_width=tbin_width,
        figsize=figsize,
        show_timestamp=show_timestamp,
    )

    for i, (tbin, ax) in enumerate(axs.items()):

        if wins is not None:
            windows_shade(
                ax,
                wins.crop_to_main(tbin, reset=True),
                window_colors=colors,
                alpha=.5,
            )

        if axhline is not None:
            ax.plot([0, tbin.length], [axhline, axhline], color='k', linewidth=1)

        if events is not None:
            plot_events_vline(ax, events[events.between(*tbin)] - tbin.start)

        for j, (name, trace) in enumerate(all_traces.items()):
            trace = trace.loc[tbin.to_slice_ms()]
            ax.plot(
                trace.index - trace.index.min(),
                trace.values,
                color=colors.get(name, f'C{j}'),
                label=name.replace('_', ' '),
                linewidth=linewidth,
                alpha=alpha
            )

        ax.tick_params(left=False, labelleft=False)
        ax.spines['left'].set_visible(False)

    ax = list(axs.values())[-1]
    ax.legend(loc='lower right', fontsize=6)

    if ylim is not None:
        ax.set_ylim(*ylim)

    set_time_axis(
        ax,
        major=major,
        minor=minor,
    )

    if suptitle is not None:
        ax.figure.suptitle(suptitle)

    return axs


def plot_wrapped_events(
        axs,
        events,
        **kwargs,
):
    """plot multiple traces, events and shaded windows for long experiments as wrapped axes"""

    for i, (tbin, ax) in enumerate(axs.items()):
        if events is not None:
            plot_events_vline(
                ax, events[events.between(*tbin)] - tbin.start,
                **kwargs,
            )


def plot_win_length_dist(wins, cats=None, scale='linear'):
    """
    Plot the distribution of window lengths
    One row per category. To show only selected categories use 'cats'.
    """

    if cats is None:
        cats = wins.wins['cat'].unique()

    vrange = wins.lengths().min(), wins.lengths().max()

    if scale == 'log':
        winlen_bins = np.geomspace(
            10 ** np.floor(np.log10(vrange[0])),
            10 ** np.ceil(np.log10(vrange[1])),
            101
        )

    else:
        winlen_bins = np.linspace(vrange[0] - 500, vrange[1] + 500, len(wins) // 3 + 1)

    f, axs = plt.subplots(
        constrained_layout=True, sharex='col', nrows=len(cats), squeeze=False, figsize=(3, 2))
    axs = axs.ravel()

    for i, cat in enumerate(cats):
        ax = axs[i]
        swins = wins.sel(cat=cat)

        ax.hist(
            swins.lengths(),
            bins=winlen_bins,
            facecolor=COLORS[cat], alpha=.5, label=cat
        )

        ax.spines['bottom'].set_position(('outward', 2))
        ax.set(ylabel='#periods')

        ax.legend(loc='upper right')

        ax.set_xscale(scale)

    ax = axs[-1]
    set_time_axis(ax, major=timeslice.ms(minutes=1), minor=timeslice.ms(seconds=5))

    # for some reason sharex can fail in log scales
    xlim = min(min(*ax.get_xlim()) for ax in axs), max(max(*ax.get_xlim()) for ax in axs)
    for ax in axs:
        ax.set_xlim(xlim)

    if scale == 'log':
        set_ticks_named_time(ax)

    else:
        ax.set_xlim(0, timeslice.ms(minutes=5))

    return axs


def plot_win_length_dist_fixwidth(rem_wins, bin_width=10_000, tmax=None, colors=None, alpha=.5, major=None, minor=None):
    # TODO merge with above
    colors = get_colors_with_defaults(colors, rem_wins['cat'])

    durations = rem_wins.lengths()

    f, ax = plt.subplots(constrained_layout=True, figsize=(2, 1.5))

    if tmax is None:
        tmax = durations.max()

    tbins = np.arange(0, tmax + bin_width, bin_width)

    for state in rem_wins['cat'].unique():
        ds = durations[rem_wins['cat'] == state]
        ax.hist(
            ds,
            facecolor=colors[state],
            bins=tbins,
            alpha=alpha,
            label=f'{state} (n={len(ds):,g})',
        )

    ax.spines['bottom'].set_position(('outward', 2))
    ax.spines['left'].set_position(('outward', 2))

    set_time_axis(ax, major=major, minor=minor)
    ax.set_ylabel('#periods')
    ax.legend(loc='upper right', fontsize=6)


def plot_traces2d_wrapped(
        stack2d, events=None,
        tbin_width=ms(hours=2.5),
        xcol='time', ycol='lag',
        grid_steps=None,
        suptitle=None,
        cmap='RdGy_r',
        norm=None,
        majory=20,
        minory=10,
        figsize=(9, 5),
        major=ms(minutes=15),
        minor=ms(minutes=5),
        show_timestamp=True,
        show_colorbar=False,
        cbar_extend='both',
        interpolation='antialiased',
):
    stack2d = stack2d.transpose(..., xcol)

    axs = make_axs_long_experiment(
        stack2d.get_rel_win(),
        tbin_width=tbin_width,
        major=major,
        minor=minor,
        show_timestamp=show_timestamp,
        figsize=figsize,
    )

    if norm is None:
        vmax = np.quantile(np.abs(stack2d.values), 0.999)
        norm = matplotlib.colors.Normalize(-vmax, vmax)

    if events is None:
        events = pd.Series([], dtype=float)

    for i, (tbin, ax) in tqdm(enumerate(axs.items()), total=len(axs)):

        s = stack2d.sel_between(time=tbin).shift_coord(time=-tbin[0])

        # we the coordinates centered on each pixel!
        # add half a bin before/after
        extent = (
            s.coords[xcol][0] * 1.5 - s.coords[xcol][1] * .5,
            s.coords[xcol][-1] * 1.5 - s.coords[xcol][-2] * .5,

            s.coords[ycol][0] * 1.5 - s.coords[ycol][1] * .5,
            s.coords[ycol][-1] * 1.5 - s.coords[ycol][-2] * .5,
        )

        im = ax.imshow(
            s.values,
            origin='lower',
            extent=extent,
            norm=norm,
            cmap=cmap,
            interpolation=interpolation,
            aspect='auto',
        )

        # imshow might change the xlim and
        # the last plot may be shorter, so make sure we show the full tbins
        ax.set_xlim(0, tbin_width)

        set_time_ticks(ax, which='y', major=majory, minor=minory)

        es = events[events.between(*tbin)] - tbin.start
        plot_events_vline(ax, es)

        if grid_steps is not None:
            for lag in grid_steps:
                ax.axhline(lag, color='xkcd:black', linewidth=.25, alpha=.5, linestyle='--')

        ax.set_xlabel('')

        if show_colorbar and i == len(axs) - 1:
            axins = inset_axes(
                ax,
                width=.05,
                height="75%",
                loc='center right',
                borderpad=2,
            )

            ax.figure.colorbar(im, ax=ax, cax=axins, extend=cbar_extend)

            axins.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(nbins=2))

    ax = list(axs.values())[-1]
    if suptitle is not None:
        ax.figure.suptitle(suptitle)

    return axs


def add_desc(ax, desc, loc='upper right', bkg_color='w', fontsize=6, loc_pad=0.05, bkg_alpha=0.75, **kwargs):
    """add a small text description on the axes, usually for n=X """

    coords = {
        'upper right': dict(x=1 - loc_pad, y=1 - loc_pad, va='top', ha='right'),
        'upper left': dict(x=0 + loc_pad, y=1 - loc_pad, va='top', ha='left'),
        'upper center': dict(x=.5, y=1 - loc_pad, va='top', ha='center'),

        'middle right': dict(x=1 - loc_pad, y=.5, va='center', ha='right'),
        'middle left': dict(x=0 + loc_pad, y=.5, va='center', ha='left'),
        'middle center': dict(x=.5, y=.5, va='center', ha='center'),

        'lower right': dict(x=1 - loc_pad, y=0 + loc_pad, va='bottom', ha='right'),
        'lower left': dict(x=0 + loc_pad, y=0 + loc_pad, va='bottom', ha='left'),
        'lower center': dict(x=.5, y=0 + loc_pad, va='bottom', ha='center'),
    }[loc]

    ax.text(
        s=desc, **coords,
        transform=ax.transAxes, fontsize=fontsize, zorder=1e6,
        bbox=dict(facecolor=bkg_color, alpha=bkg_alpha, edgecolor='none'),
        **kwargs,
    )


def get_extent(s: Stack, xcol=None, ycol=None) -> tuple:
    """
    Extract the extent to use with imshow when trying
    to plot a stack as a 2d image.
    Note that the stack must have a fixed sampling period
    along the x and y dimensions.
    """
    if xcol is None:
        xcol = s.get_coords_names()[0]

    if ycol is None:
        ycol = s.get_coords_names()[1]

    x_period = s.estimate_sampling_period(xcol)
    y_period = s.estimate_sampling_period(ycol)

    x_win = s.get_rel_win(xcol)
    y_win = s.get_rel_win(ycol)

    return (
        x_win.start - x_period * .5,
        x_win.stop - x_period * .5,
        y_win.start - y_period * .5,
        y_win.stop - y_period * .5,
    )


def filter_desc(hz: tuple) -> str:
    if hz is None:
        return 'raw'

    low, high = hz
    low_open = low is None or np.isclose(low, 0) or np.isinf(low) or np.isnan(low)
    high_open = high is None or np.isclose(high, 0) or np.isinf(high) or np.isnan(high)

    if low_open and high_open:
        return 'raw'
    else:
        if low_open:
            return f'<{high}hz'
        elif high_open:
            return f'>{low}hz'

        else:
            return f'{low}-{high}hz'


def get_ax_with_marginals(figsize=(3, 2), constrained_layout=True, size_ratio=3):
    from matplotlib.gridspec import GridSpec

    f = plt.figure(
        figsize=figsize,
        constrained_layout=constrained_layout
    )

    gs = GridSpec(
        nrows=2,
        ncols=2,
        figure=f,
        width_ratios=[size_ratio, 1],
        height_ratios=[1, size_ratio],
    )

    axs_dict = {
        'main': f.add_subplot(gs[1, 0]),
        'xmargin': f.add_subplot(gs[0, 0]),
        'ymargin': f.add_subplot(gs[1, 1]),
    }

    axs_dict['main'].get_shared_x_axes().join(axs_dict['main'], axs_dict['xmargin'])
    axs_dict['main'].get_shared_y_axes().join(axs_dict['main'], axs_dict['ymargin'])

    axs_dict['ymargin'].tick_params(left=False, labelleft=False)
    axs_dict['xmargin'].tick_params(bottom=False, labelbottom=False)

    return axs_dict


def align_axes(ax0, ax1, which='x'):
    """
    This will break if constrained_layout is active

    :param ax0:
    :param ax1:
    :param which:
    :return:
    """

    bbox0 = ax0.get_position()
    bbox1 = ax1.get_position()

    if which == 'x':
        which = 0
    else:
        assert which == 'y'
        which = 1

    v0 = max(bbox0.p0[which], bbox1.p0[which])
    v1 = min(bbox0.p1[which], bbox1.p1[which])

    for ax in [ax0, ax1]:
        bbox = ax.get_position().get_points()

        bbox[0][which] = v0
        bbox[1][which] = v1

        ax.set_position(matplotlib.transforms.Bbox(bbox))


def get_colors_with_defaults(given, states) -> dict:
    if given is None:
        given = {}

    for i, state in enumerate(np.unique(states)):
        given.setdefault(state, COLORS.get(state, f'C{i}'))

    return given


def plot_scat_with_marginals(
        samples,
        by=None,
        alpha=1,
        s=.5,
        figsize=(3, 3),
        colors=None,
        suptitle='',
        density=False,
        xlabel=None,
        ylabel=None,
        rasterized=False,
        constrained_layout=True, size_ratio=3,
        clip_on=True,
        bin_count=50,
        **kwargs,
):
    assert samples.shape[1] == 2

    if by is None:
        by = ['none'] * len(samples)

    if not isinstance(by, pd.Series):
        by = pd.Series(np.asarray(by), index=samples.index)

    colors = get_colors_with_defaults(colors, by)

    axs = get_ax_with_marginals(
        figsize=figsize,
        constrained_layout=constrained_layout, size_ratio=size_ratio,
    )

    ax = axs['main']

    xcol = samples.columns[0]
    ycol = samples.columns[1]

    ax.scatter(
        samples[xcol],
        samples[ycol],
        facecolor=by.map(colors),
        s=s,
        alpha=alpha,
        rasterized=rasterized,
        clip_on=clip_on,
        **kwargs,
    )

    for i, ch in enumerate([xcol, ycol]):

        loc = 'xmargin' if i == 0 else 'ymargin'

        bins = np.linspace(
            samples[ch].replace(-np.inf, np.nan).min(),
            samples[ch].replace(+np.inf, np.nan).max(),
            bin_count + 1,
        )

        ax = axs[loc]
        for state in by.unique():
            ax.hist(
                samples.loc[by == state, ch],
                facecolor=colors[state],
                alpha=.5,
                density=density,
                bins=bins,
                orientation='horizontal' if loc == 'ymargin' else 'vertical',
                label=state,
            )

            if loc == 'ymargin':
                ax.spines['left'].set_position(('outward', 2))
                ax.set_xlabel('prob' if density else 'count')

            else:
                ax.spines['bottom'].set_position(('outward', 2))
                ax.set_ylabel('prob' if density else 'count')

    ax = axs['main']

    if xlabel is None:
        xlabel = xcol.replace('_', ' ')

    if ylabel is None:
        ylabel = ycol.replace('_', ' ')

    ax.set(xlabel=xlabel, ylabel=ylabel)

    ax.figure.suptitle(suptitle)

    return axs


def plot_scat_matrix_by(df, by=None, s=.25, edgecolor='none', figsize=(7, 7), **kwargs):
    if by is None:
        by = ['none'] * len(df)

    if not isinstance(by, pd.Series):
        by = pd.Series(np.asarray(by), index=df.index)

    by = by.loc[df.index]

    colors = get_colors_with_defaults(None, by)

    f, axs = plt.subplots(
        nrows=len(df.columns) - 1,
        ncols=len(df.columns) - 1,
        sharex='col', sharey='row', constrained_layout=True,
        figsize=figsize,
    )

    for ax in axs.ravel():
        ax.axis('off')

    for (i0, c0), (i1, c1) in itertools.combinations(enumerate(df.columns), 2):
        ax = axs[i1 - 1, i0]
        ax.axis('on')

        ax.scatter(
            df[c0],
            df[c1],
            s=s,
            edgecolor=edgecolor,
            facecolor=by.map(colors),
            **kwargs,
        )

        if i0 == 0:
            ax.set_ylabel(c1)

        if i1 == axs.shape[0]:
            ax.set_xlabel(c0)


def plot_trace_highlighted(ax, trace: pd.Series, wins: timeslice.Windows, styles: dict, **kwargs):
    """
    Plot a trace in sections given by some windows.
    The style of each section is determined by the category of each window
    """
    assert wins.are_exclusive()

    # noinspection PyTypeChecker
    cropped = wins.crop_df(trace, show_pbar=len(wins) > 1000, reset=None).items()

    if len(cropped) > 1000:
        cropped = tqdm(cropped, desc='plot')

    for win_idx, trace in cropped:
        ax.plot(trace, **styles[wins['cat'][win_idx]], **kwargs)
