import numpy as np

from ihrem import stacks


def pick_beta_periods(
        loader,
        beta_trace,
        count,
        quantile=.9,
        win=(-500, 500),
        mode='high',
        zscored=True,
) -> stacks.Stack:
    """
    
    :param loader: 
    :param beta_trace: 
    :param count: 
    :param quantile: 
    :param win: 
    :param mode: 
    :param zscored: 
    :return: 
    """

    if mode == 'high':
        candidante_times = beta_trace.index[beta_trace > beta_trace.quantile(quantile)]
    else:
        candidante_times = beta_trace.index[beta_trace < beta_trace.quantile(quantile)]

    times = candidante_times[(np.linspace(0, 1, count + 2)[1:-1] * len(candidante_times)).astype(int)]

    s = stacks.Stack.load_ms(
        loader,
        times.values,
        win_ms=win,
    )

    if zscored:
        mean = s.mean(('win_idx', 'time'))
        std = s.std(('win_idx', 'time'))
        s = (s - mean) / std

    s = s.replace_dim('win_idx', 'ref', times)

    return s
