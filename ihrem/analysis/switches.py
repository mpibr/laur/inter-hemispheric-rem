import matplotlib.colors
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem import stacks, timeslice, events
from ihrem.analysis import sne
from ihrem.timeslice import ms


def plot_classification_detailed(
        stack2d, fit_traces=None, wins=None, colors=None, zoomin=None,
        figsize=(7, 3.5),
        tracey=(.03, .4),
        tbin_width=ms(hours=2),
        linewidth_wins=3,
        markers=None,
        show_colorbar=True,
):
    axs = splot.plot_traces2d_wrapped(
        stack2d,
        tbin_width=tbin_width,
        figsize=figsize,
        show_timestamp=zoomin is None,
        show_colorbar=show_colorbar,
    )

    if fit_traces is not None:
        max_trace = np.quantile(fit_traces.values.ravel(), q=.999)
        min_trace = np.quantile(fit_traces.values.ravel(), q=.001)

        norm_traces = (fit_traces - min_trace) / (max_trace - min_trace)
        norm_traces = norm_traces * (tracey[1] - tracey[0]) + tracey[0]

        for tbin, ax in axs.items():
            for col, trace in norm_traces.items():
                trace = tbin.crop_df(trace, reset='start')
                ax.plot(
                    trace,
                    clip_on=True,
                    zorder=1e10,
                    color=splot.COLORS[col],
                    linewidth=.5,
                    transform=ax.get_xaxis_transform(),
                )

    if markers is not None:
        for tbin, ax in axs.items():
            isin = (tbin.start <= markers) & (markers <= tbin.stop)

            win_markers = markers[isin]
            ax.scatter(
                win_markers - tbin.start,
                np.ones(len(win_markers)) * .01,
                marker='|',
                linewidth=.5,
                s=20,
                color='k',
                transform=ax.get_xaxis_transform(),
                clip_on=True,
                zorder=1e7,
            )

    if wins is not None:
        colors = splot.get_colors_with_defaults(colors, wins['cat'])

        for tbin, ax in axs.items():
            for cat in wins.wins['cat'].unique():
                sel_wins = wins.crop_to_main(tbin, reset=True).sel(cat=cat)

                if len(sel_wins) > 0:
                    ax.plot(
                        sel_wins.wins[['start', 'stop']].T,
                        [0.01, 0.01],
                        color=colors[cat],
                        transform=ax.get_xaxis_transform(),
                        linewidth=linewidth_wins,
                        clip_on=True,
                        zorder=1e6,
                        alpha=.75,
                        solid_capstyle='butt',
                    )

    if zoomin is not None:
        list(axs.values())[-1].set_xlim(*zoomin)

    list(axs.values())[-1].spines['bottom'].set_visible(True)

    return axs


def extract_lead_traces(xcorr, rem_wins, all_beta, lag_modes):
    lead_traces: pd.DataFrame = xcorr.sel(lag=list(lag_modes)).to_dataframe2d().T
    lead_traces.columns = ['lead_ch1', 'lead_ch0']

    lead_traces['lead_diff'] = lead_traces['lead_ch1'] - lead_traces['lead_ch0']

    # we extract power at a lower sampling rate, so make sure we cover the same space
    lead_traces = rem_wins.get_global_win().crop_df(lead_traces)
    lead_traces['beta_ch0'] = events.interpolate_trace(all_beta['ch0'], lead_traces.index)
    lead_traces['beta_ch1'] = events.interpolate_trace(all_beta['ch1'], lead_traces.index)
    lead_traces['rem_state'] = rem_wins.generate_cat(lead_traces.index)
    lead_traces['log_lead_ch0'] = np.log10(lead_traces['lead_ch0'])
    lead_traces['log_lead_ch1'] = np.log10(lead_traces['lead_ch1'])

    return lead_traces


def load_lead_traces(reg, exp_name, tau=1_000, exp_valid_win=timeslice.Win(ms(hours=2), ms(hours=2 + 9)), low_hz=100):
    all_beta = reg.load_all_beta_norm(exp_name, exp_valid_win=exp_valid_win)

    rem_wins = extract_rem_wins_from_log_beta_thresh_detour(all_beta)

    xcorr = load_xcorr(reg, exp_name, tau=tau, suffix='_exp_clipped', exp_valid_win=exp_valid_win, low_hz=low_hz)

    lag_modes = extract_lag_modes(xcorr)

    lead_traces = extract_lead_traces(xcorr, rem_wins, all_beta, lag_modes)
    lead_traces['lead_state_thresh'] = classify_lead_state_by_diag_thresh(lead_traces)
    lead_traces['lead_state'] = smooth_lead_state_detour(lead_traces['lead_state_thresh'])

    return lead_traces


def extract_lag_modes(xcorr):
    best_lags = xcorr.idxmax('lag').to_series()

    lag_modes = (
        best_lags[best_lags < 0].mode().max(),
        best_lags[best_lags > 0].mode().min(),
    )

    return lag_modes


def plot_mean_by(xcorr, lead_states, ylabel='', title='', colors=None, figsize=(3, 2)):
    colors = splot.get_colors_with_defaults(colors, lead_states)

    f, ax = plt.subplots(constrained_layout=True, figsize=figsize)

    xlabel = xcorr.get_coords_names_except('time')
    assert len(xlabel) == 1, xlabel
    xlabel = xlabel[0]

    ax.set(
        xlabel=xlabel,
        ylabel=ylabel,
        title=title,
    )

    for state in lead_states.unique():
        ax.plot(
            xcorr.sel(time=lead_states.index[lead_states == state]).mean('time').to_series(),
            color=colors[state],
        )


def load_xcorr(reg, exp_name, tau, suffix='_exp', exp_valid_win=None, low_hz=100):
    sliding_win = tau * 10

    xcorr = stacks.Stack.load_hdf(
        reg.get_path_xcorr_area(exp_name, area='CLA', sliding_win=sliding_win, suffix=suffix, low_hz=low_hz),
        'xcorr'
    )

    if exp_valid_win is not None:
        xcorr = xcorr.sel_between(time=exp_valid_win)

    vmax = np.quantile(xcorr.values.ravel(), .999)
    assert vmax > 0
    xcorr = xcorr / vmax

    return xcorr


def get_beta_log(all_beta):
    beta_fit_traces = np.log10(all_beta[['ch0', 'ch1']])
    beta_fit_traces.columns = ['log(beta_ch0)', 'log(beta_ch1)']
    return beta_fit_traces


def extract_rem_wins_from_log_beta_thresh_detour(all_beta, beta_thresh=.15, max_detours=ms(seconds=15)):
    is_rem = all_beta['beta_max'] > beta_thresh

    rem_states = is_rem.map({True: 'rem', False: 'sws'})

    rem_wins = timeslice.Windows.build_from_contiguous_values(rem_states, include_right=False)

    rem_wins = rem_wins.merge_sandwiched(max_length=max_detours)

    return rem_wins


def plot_lead_traces_state(
        lead_traces, by, total=500_000, s=2 ** -2, names=('lead_ch0', 'lead_ch1'), suptitle='',
        alpha=1,
        figsize=(3, 3),
        xlabel='+20ms x-corr',
        ylabel='-20ms x-corr',
        show_legend=True
):
    shown = lead_traces.iloc[:total:5]

    axs = splot.plot_scat_with_marginals(
        shown[list(names)],
        alpha=alpha,
        figsize=figsize,
        by=shown[by],
        s=s,
        suptitle=suptitle,
        xlabel=xlabel,
        ylabel=ylabel,
    )

    if show_legend:
        axs['xmargin'].legend(fontsize=6)

    return axs


def classify_lead_state_by_diag_thresh(lead_traces, col0='log_lead_ch0', col1='log_lead_ch1', slope=1.2):
    lead_states = pd.Series('x', index=lead_traces.index)

    sws_center = lead_traces.loc[lead_traces['rem_state'] == 'sws', [col0, col1]].mean()

    mask = (lead_traces[col0] - sws_center[col0]) * slope + sws_center[col0] < lead_traces[col1]
    lead_states.loc[mask] = 'lead_ch1'

    mask = (lead_traces[col1] - sws_center[col1]) * slope + sws_center[col1] < lead_traces[col0]
    lead_states.loc[mask] = 'lead_ch0'

    lead_states.loc[lead_traces['rem_state'] == 'sws'] = 'sws'

    return lead_states


def extract_cycle_wins(rem_wins, rem_align=.5, sws_align=None):
    """
    Create windows that go from SWS to SWS including exactly one REM
    in each window.
    Reference time is extracted relative to this REM center.
    """

    if sws_align is None:
        sws_align = 1 - rem_align

    rem = rem_wins.sel(cat='rem')

    sws = rem_wins.sel(cat='sws')
    breaks = sws.relative_time(sws_align)

    if rem_wins['cat'].values[0] == 'rem':
        breaks = np.append(rem_wins['start'].min(), breaks)

    if rem_wins['cat'].values[-1] == 'rem':
        breaks = np.append(breaks, rem_wins['stop'].max())

    cycle_wins = timeslice.Windows.build_between(breaks)

    cycle_wins['ref'] = rem.relative_time(rem_align).values
    assert cycle_wins.is_ref_inside().all()
    cycle_wins['rem_idx'] = rem.index

    return cycle_wins.rename_index('cycle_idx')


def classify_wins_by_cycle(lead_wins_raw, cycle_wins):
    lead_wins: timeslice.Windows = lead_wins_raw.crop_to_multiple(cycle_wins, drop=True)
    lead_wins = lead_wins.sel_mask(lead_wins.lengths() > 0)
    lead_wins['ref'] = lead_wins.mid()

    lead_wins: pd.DataFrame = cycle_wins.rename_index('idx').annotate_events(lead_wins.wins, col='ref', prefix='cycle')
    lead_wins.rename(columns=dict(cycle_rem_idx='rem_idx'), inplace=True)
    lead_wins = lead_wins.dropna()
    lead_wins['cycle_idx'] = lead_wins['cycle_idx'].astype(int)
    lead_wins = timeslice.Windows(lead_wins)

    assert lead_wins.are_exclusive() and lead_wins.are_tight()
    return lead_wins


def plot_all_lead_state_cycles(all_lead_state_cycles):
    f, axs = plt.subplots(
        figsize=(1 + len(all_lead_state_cycles) * 8 / 6, 6), constrained_layout=True, ncols=len(all_lead_state_cycles),
        sharex='all', sharey='all', squeeze=False)

    for j, (name, lead_state_cycles_wins) in enumerate(all_lead_state_cycles.items()):

        ax = axs[0, j]

        for i, (idx, wins) in enumerate(pbar(lead_state_cycles_wins.items())):
            splot.plot_windows_line(
                ax,
                wins,
                i,
            )

        ax.set_ylim(-1, len(lead_state_cycles_wins) + 1)
        splot.set_time_axis(ax, major=ms(minutes=1), minor=ms(minutes=.5), show_unit=False)
        ax.set_xlabel(f'time rel to REM\n{name} (minutes)')

    axs[0, 0].invert_yaxis()
    axs[0, 0].set_ylabel('cycle (chronologically)')

    return axs


def plot_lead_state_cycle_counts(counts, suptitle=''):
    combs2d = [
        ('lead_ch0', 'lead_ch1'),
        #         ('x', 'lead_ch0'),
        #         ('x', 'lead_ch1'),
    ]

    f, axs = plt.subplots(constrained_layout=True, ncols=1 + len(combs2d), figsize=(1 + 2 * len(combs2d), 1.75))

    f.suptitle(suptitle)

    ax = axs[0]

    total_good = counts['lead_ch0'] + counts['lead_ch1']

    ax.hist(
        total_good - 1,
        bins=np.arange(-.5, total_good.max() + 1.5, 1),
        facecolor='k'
    )

    ax.set_ylabel('#rem periods')
    ax.set_xlabel('# switches')
    ax.set_xticks(np.arange(0, 9, 1), minor=False)
    ax.spines['bottom'].set_position(('outward', 2))

    vmax = 100

    for i, (a, b) in enumerate(combs2d):
        ax = axs[1 + i]

        h, _, _, im = ax.hist2d(

            counts[a],
            counts[b],
            bins=[np.arange(-.5, 9, 1)] * 2,
            cmap='cividis',
            norm=matplotlib.colors.Normalize(0, vmax),
        )
        ax.set_xticks(np.arange(0, 9, 1), minor=False)
        ax.set_yticks(np.arange(0, 9, 1), minor=False)
        ax.tick_params(bottom=False, left=False)

        ax.set_aspect('equal')

        f.colorbar(im, ax=ax, aspect=70).set_label('#rem periods')

        ax.set_ylabel(f'#periods "{a}"'.replace('_', ' '), color=splot.COLORS[a])
        ax.set_xlabel(f'#periods "{b}"'.replace('_', ' '), color=splot.COLORS[b])

    return axs


def smooth_lead_state_detour(lead_state_raw):
    lead_wins_raw = timeslice.Windows.build_from_contiguous_values(lead_state_raw)

    lead_wins_0 = None
    lead_wins = lead_wins_raw.copy()

    while lead_wins_0 is None or len(lead_wins_0) != len(lead_wins):
        lead_wins_0 = lead_wins
        lead_wins = lead_wins.merge_sandwiched(cat='x', max_length=15_000)
        lead_wins = lead_wins.merge_sandwiched(cat=['lead_ch0', 'lead_ch1'], max_length=5_000)

    return lead_wins.generate_cat(lead_state_raw.index)


def plot_cycle_traces(exp_name, lead_state_cycles, ):
    lead_vrange = 0, 1

    div_ch_cmap = matplotlib.colors.LinearSegmentedColormap.from_list(
        '', [splot.COLORS['ch0'], 'w', splot.COLORS['ch1']])

    to_plot = {
        'beta_max': dict(
            cmap='cividis',
            norm=matplotlib.colors.Normalize(0, 1),
        ),
        'lead_ch0': dict(
            cmap=splot.CMAPS_CHANNEL_WHITE[f'ch0'],
            norm=matplotlib.colors.Normalize(*lead_vrange),
        ),
        'lead_ch1': dict(
            cmap=splot.CMAPS_CHANNEL_WHITE[f'ch1'],
            norm=matplotlib.colors.Normalize(*lead_vrange),
        ),
        'lead_diff': dict(
            cmap=div_ch_cmap,
            norm=matplotlib.colors.Normalize(-1, 1),
        ),
        'lead_score': dict(
            cmap=div_ch_cmap,
            norm=matplotlib.colors.Normalize(-.5, +.5),
        ),
    }

    f, axs = plt.subplots(constrained_layout=True, figsize=(8, 4), nrows=len(to_plot), sharex='all', sharey='all')

    f.suptitle(exp_name)

    for k, (col, kwargs) in pbar(enumerate(to_plot.items())):
        ax = axs[k]

        im = None
        for i, traces in pbar(lead_state_cycles.items()):
            trace = traces[col]

            xextent = trace.index.min(), trace.index.max()
            yextent = (i, i + 1)

            im = ax.imshow(
                X=trace.values[::-1].reshape(-1, 1),
                extent=tuple(yextent) + tuple(xextent),
                interpolation='nearest',
                **kwargs,
            )

        if im is not None:
            f.colorbar(im, ax=ax, aspect=70)

        ax.set_xlim(-5, len(lead_state_cycles) + 5)

        splot.set_time_axis(ax, which='y', show_unit=False)
        ax.set_ylim(ms(minutes=-2), ms(minutes=2))
        splot.add_desc(ax, col.replace('_', ' '))

    for ax in axs:
        ax.set_ylabel('min')
    axs[-1].set_xlabel('# cycle')


class Cycles:
    """
    Represent a 2-level hyerarchy of windows where the
    first level represents cycles of rem-swsget_rem_wins
    and the second level subdivides those cycles into leadership periods.
    """

    def __init__(self, lead_wins):
        self.lead_wins = lead_wins
        assert 'cycle_idx' in self.lead_wins.wins.columns

    @classmethod
    def from_rem_wins(cls, lead_wins, rem_wins, rem_align=.5, sws_align=0.5):
        cycle_wins = extract_cycle_wins(rem_wins, rem_align=rem_align, sws_align=sws_align)
        classified_lead_wins = classify_wins_by_cycle(lead_wins, cycle_wins)
        return cls(classified_lead_wins)

    def shift(self, value):
        return self.__class__(
            self.lead_wins.shift(value)
        )

    def sel_mask_cycle(self, mask):
        return self.__class__(
            self.lead_wins.sel_mask(
                self.lead_wins['cycle_idx'].map(mask)
            )
        )

    def is_clean_switch(self) -> pd.Series:
        counts = self.get_period_counts_per_cycle()
        return (
                counts['lead_ch0'].isin([0, 1]) &
                counts['lead_ch1'].isin([0, 1])
        )

    def is_single_switch(self) -> pd.Series:
        counts = self.get_period_counts_per_cycle()
        return (counts[['lead_ch0', 'lead_ch1']] == 1).all(axis=1) & (counts['x'] == 1)

    def align_to_switch(self):
        rem_centers = self.get_rem_center()
        switch_times = self.get_switch_times()

        shifts = switch_times.copy()
        shifts.loc[shifts.isna()] = rem_centers.loc[shifts.isna()]

        return self.align_to(shifts)

    def align_to_rem_center(self):
        rem_centers = self.get_rem_center()
        return self.align_to(rem_centers)

    def align_to(self, rem_times):

        lead_wins_shifted = self.lead_wins.shift(
            -1 * self.lead_wins['cycle_idx'].map(rem_times)
        )
        return self.__class__(
            lead_wins_shifted,
        )

    def get_period_center_of_mass_per_cycle(self) -> pd.DataFrame:
        df = self.lead_wins.wins.copy()
        df['length'] = self.lead_wins.lengths()

        df['weighted_length'] = df['length'] * self.lead_wins.mid()

        weighted_sums = df.groupby(['cat', 'cycle_idx'])['weighted_length'].sum()
        lengths = df.groupby(['cat', 'cycle_idx'])['length'].sum()

        com = (weighted_sums / lengths).unstack('cat')

        return com

    def get_period_center_of_mass_per_cycle_by_sampling(self) -> pd.DataFrame:
        com = {}

        for idx, wins in pbar(self.lead_wins.groupby('cycle_idx')):
            samples = timeslice.Windows(wins).generate_cat_contiguous(1)
            com[idx] = samples.rename_axis(index='time').reset_index(name='cat').groupby('cat')['time'].mean()

        com = pd.DataFrame.from_dict(com, orient='index').sort_index()
        com.rename_axis(index='cycle_idx', inplace=True)

        return com

    def get_period_counts_per_cycle(self) -> pd.DataFrame:
        """
        Number of periods in each cycle. Result looks like:

            cat        lead_ch0  lead_ch1  sws  x
            cycle_idx
            0                 3         2    2  4
            1                 3         2    2  4
        """
        return self.lead_wins.groupby(['cycle_idx', 'cat']).size().unstack('cat', fill_value=0)

    def get_switch_count_per_cycle(self) -> pd.Series:
        return self.get_period_counts_per_cycle()[['lead_ch0', 'lead_ch1']].sum(axis=1) - 1

    def get_period_lengths_per_cycle(self) -> pd.DataFrame:
        df = self.lead_wins.wins.copy()
        df['length'] = self.lead_wins.lengths()
        return df.groupby(['cat', 'cycle_idx'])['length'].sum().unstack('cat', fill_value=0)

    def get_period_lengths_relative(self, valid_cats=('lead_ch0', 'lead_ch1')) -> pd.Series:
        lengths = self.get_period_lengths_per_cycle()
        lengths = lengths.loc[:, list(valid_cats)]
        total = lengths.sum(axis=1)
        return (lengths.T / total).T.dropna()

    def take_largest_by_cat(self):
        df = self.lead_wins.wins.copy()
        df['length'] = self.lead_wins.lengths()

        largest = df.groupby(['cat', 'cycle_idx'])['length'].idxmax()
        assert largest.is_unique

        return self.__class__(
            self.lead_wins.sel_mask(largest.sort_values().values)
        )

    def get_center_largest(self) -> pd.DataFrame:
        largest = self.take_largest_by_cat()
        assert np.all(largest.lead_wins.groupby(['cycle_idx', 'cat']).size() <= 1)
        return largest.lead_wins.groupby(['cycle_idx', 'cat'])['ref'].first().unstack('cat')

    def get_switch_periods(self) -> timeslice.Windows:
        """
        Extract up to one window per cycle representing a switch.
        This will use the largest period per category and detect the gap time
        between the biggest lead_ch0 and lead_ch1.
        Note this likely only makes sense for 1-switch cycles.
        0-switch cycles will be ignored.
        :return:
        """

        switch_wins = {}

        for idx, periods in self.take_largest_by_cat().lead_wins.wins.groupby('cycle_idx'):
            assert periods['cat'].is_unique

            periods = periods.set_index('cat')

            if 'lead_ch0' in periods.index and 'lead_ch1' in periods.index:

                if periods.loc['lead_ch0', 'ref'] < periods.loc['lead_ch1', 'ref']:
                    switch_win = (periods.loc['lead_ch0', 'stop'], periods.loc['lead_ch1', 'start'])

                else:
                    switch_win = (periods.loc['lead_ch1', 'stop'], periods.loc['lead_ch0', 'start'])

                switch_wins[idx] = switch_win

        switch_wins = pd.DataFrame.from_dict(switch_wins, orient='index', columns=['start', 'stop'])

        switch_wins['ref'] = switch_wins.mean(axis=1)

        return timeslice.Windows(switch_wins)

    def get_switch_times(self) -> pd.Series:
        return self.get_switch_periods()['ref']

    def get_rem_wins(self):
        rems = pd.DataFrame({
            'start': self.lead_wins.sel_except(cat='sws').groupby('cycle_idx')['start'].min(),
            'stop': self.lead_wins.sel_except(cat='sws').groupby('cycle_idx')['stop'].max(),
        })

        rems['ref'] = rems.mean(axis=1)

        return timeslice.Windows(rems)

    def get_cycle_wins(self):
        cycles = pd.DataFrame({
            'start': self.lead_wins.groupby('cycle_idx')['start'].min(),
            'stop': self.lead_wins.groupby('cycle_idx')['stop'].max(),
        })

        cycles['ref'] = self.get_rem_center()

        return timeslice.Windows(cycles)

    def get_rem_center(self) -> pd.Series:
        return self.get_rem_wins().mid()

    def get_cycle_direction(self) -> pd.Series:
        """
        Only valid for single-switch cycles.
        Get True if switch is ch1 to ch0. False otherwise.
        :return:
        """
        switch_periods = self.get_switch_periods()
        aligned = self.align_to(switch_periods['ref'])
        sorting = aligned.get_period_center_of_mass_per_cycle()
        # noinspection PyTypeChecker
        return sorting['lead_ch1'] < sorting['lead_ch0']

    def plot_cycles_stack(self, ax, cycle_idcs=None, height=.8):
        grouped = self.lead_wins.groupby('cycle_idx')

        if cycle_idcs is None:
            cycle_idcs = np.sort(self.lead_wins['cycle_idx'].unique())

        for k, cycle_idx in enumerate(pbar(cycle_idcs)):
            wins = grouped.get_group(cycle_idx)
            y = k

            splot.windows_shade(
                ax,
                wins,
                ymin=y - .5 * height,
                ymax=y + .5 * height,
                transform=ax.transData,
                alpha=.5,
            )

    def classify_sns(self, matched):
        cycle_wins = self.get_cycle_wins()
        sn_cycle = cycle_wins.classify_events(matched.reg['ref_time'])
        assert sn_cycle.index.is_unique
        sn_cycle.rename(columns=dict(delay='cycle_delay'), inplace=True)

        sns = pd.concat([matched.reg, sn_cycle], axis=1).dropna()
        sns = sne.SharpNegativeEvents(sns)
        sns = sns.sel(is_lead=True)

        return sns
