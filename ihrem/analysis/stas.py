"""fast all-to-all STAs (Spike-Triggered-Averages) between cells"""
import itertools
from pathlib import Path

import h5py
import numba as nb
import numpy as np
from tqdm.auto import tqdm as pbar

from ihrem import trains


class MultiCellStore:
    """store STA for tuples of cells in hdf5"""

    # see https://stackoverflow.com/a/15704334/22609 for a way to turn this into a parallel write

    def __init__(self, path, desc):
        self.path = Path(path)
        self.desc = desc

    def _get_key(self, gids: tuple):
        prefix = self.desc + '_'
        key = prefix + '_'.join(f'{gid:03d}' for gid in gids)
        return key

    def add(self, gids: tuple, data: np.array, **attr):
        key = self._get_key(gids)
        with h5py.File(str(self.path), mode='a') as f:
            dataset = f.create_dataset(key, data=data)

            for k, v in attr.items():
                dataset.attrs[k] = v

    def get(self, gids: tuple):
        key = self._get_key(gids)
        with h5py.File(str(self.path), mode='r') as f:
            data = np.array(f[key])
        return data

    def get_attr(self, gids: tuple):
        key = self._get_key(gids)
        with h5py.File(str(self.path), mode='r') as f:
            attr = dict(f[key].attrs)
        return attr

    def get_existing(self):
        """return a list of existing tuples in the storage"""
        existing = []

        if self.path.exists():
            prefix = self.desc + '_'

            with h5py.File(str(self.path), mode='r') as f:
                for key in f.keys():
                    if key.startswith(prefix):
                        key = key[len(prefix):]
                        existing.append(tuple(int(gid) for gid in key.split('_')))

        return existing


def generate_all_combs(unique_gids):
    combs = itertools.product(unique_gids, itertools.permutations(unique_gids, 2))
    combs = [(gid0, gid1, gid2) for gid0, (gid1, gid2) in combs if (gid0 != gid1) and (gid0 != gid2)]
    return combs


@nb.njit
def _find_spikes_in_win(from_idx, ts, win_start, win_stop):
    """
    Return the range of indices [start, stop) on the array of times ts so that all
    values in the range fall within [win_start, win_stop).

    ts must have been sorted.

    We start searching for the range at index from_idx.
    We can implement a sliding window efficiently by calling this function
    repeatedly using the last returned start_idx as the new from_idx.
    """

    size = len(ts)

    start_idx = from_idx

    while (start_idx + 1) < size and ts[start_idx] < win_start:
        start_idx += 1

    stop_idx = start_idx

    while (stop_idx + 1) < size and ts[stop_idx] < win_stop:
        stop_idx += 1

    # assert start <= stop
    # assert len(ts[start:stop]) == 0 or (win_start <= np.min(ts[start:stop]) < win_stop)
    # assert len(ts[start:stop]) == 0 or (win_start <= np.max(ts[start:stop]) < win_stop)

    return start_idx, stop_idx


@nb.njit
def _count_triplets(t0s, t1s, t2s, tbins):
    h_sum = np.zeros(
        (len(tbins) - 1, len(tbins) - 1),
        dtype=np.uint8,
    )

    t1_start_idx, t1_stop_idx = 0, 0
    t2_start_idx, t2_stop_idx = 0, 0

    win = np.min(tbins), np.max(tbins)

    for t0 in t0s:
        win_start, win_stop = t0 + win[0], t0 + win[1]

        t1_start_idx, t1_stop_idx = _find_spikes_in_win(t1_start_idx, t1s, win_start, win_stop)
        d1s = t1s[t1_start_idx:t1_stop_idx] - t0
        i1s = np.digitize(d1s, tbins) - 1
        # assert np.all(0 <= i1s) and np.all(i1s < len(tbins) - 1)

        t2_start_idx, t2_stop_idx = _find_spikes_in_win(t2_start_idx, t2s, win_start, win_stop)
        d2s = t2s[t2_start_idx:t2_stop_idx] - t0
        i2s = np.digitize(d2s, tbins) - 1
        # assert np.all(0 <= i2s) and np.all(i2s < len(tbins) - 1)

        for i1 in i1s:
            for i2 in i2s:
                h_sum[i1, i2] = h_sum[i1, i2] + 1

    return h_sum


def extract_sta_triplets(spikes, tbins, output_path):
    spikes = spikes.sort_values(['time', 'gid'])
    unique_gids = spikes['gid'].unique()
    grouped_spikes = spikes.groupby('gid')['time']

    all_combs = generate_all_combs(unique_gids)

    store = MultiCellStore(output_path, desc='sta')

    existing = store.get_existing()
    remaining = set(all_combs).difference(set(existing))
    if len(existing) > 0:
        print(f'Ignoring {len(existing):,d}/{len(all_combs):,d} combinations that exist already in storage.',
              flush=True)

    try:
        for i, triplet in enumerate(pbar(remaining, desc='comb')):
            if triplet not in existing:
                (gid0, gid1, gid2) = triplet

                spikes0 = grouped_spikes.get_group(gid0).sort_values().values
                spikes1 = grouped_spikes.get_group(gid1).sort_values().values
                spikes2 = grouped_spikes.get_group(gid2).sort_values().values

                h = _count_triplets(spikes0, spikes1, spikes2, tbins)

                store.add(triplet, h, win_start=np.min(tbins), win_stop=np.max(tbins))

    except KeyboardInterrupt:
        print('quit early')


def main():
    spikes = trains.SpikeTrains.load('data/example_spks.h5', win_ms=(-100, 100)).spikes
    tbins = np.arange(-100, 101, 1)

    extract_sta_triplets(spikes, tbins, output_path='./data/triplet_stas.h5')


if __name__ == '__main__':
    main()
