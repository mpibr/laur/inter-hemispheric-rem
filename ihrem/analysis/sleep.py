"""
Analyse dragon sleep.
"""
import logging

import numba
import numpy as np
import pandas as pd
import scipy.integrate
import scipy.signal
import scipy.stats
from tqdm.auto import tqdm as pbar

from ihrem import stacks, timeslice
from ihrem.stacks import Stack
from ihrem.timeslice import S_TO_MS, MS_TO_S

####################################################################################
# General definitions

# nice tutorial: https://raphaelvallat.com/bandpower.html
FREQ_BANDS = pd.DataFrame.from_dict({
    'delta': (0.5, 4, 0),
    'theta': (4, 8, 1),
    'alpha': (8, 12, 2),
    'beta': (12, 30, 3),
    'gamma': (30, 100, 4),
    'spiking': (300, 2_000, 6),
    'total': (0, np.inf, 5),
}, orient='index', columns=['freq_min', 'freq_max', 'freq_idx'])
assert FREQ_BANDS['freq_idx'].is_unique

FREQ_BAND_COLORS = {band: f'C{i}' for i, band in enumerate(FREQ_BANDS.index)}
FREQ_BAND_COLORS['total'] = 'black'

MODE_COLORS = dict(
    sws='xkcd:cerulean',
    rem='xkcd:green',
    unknown='xkcd:silver',
    any='xkcd:charcoal',
)


def band_name_to_idx(power: Stack):
    """convert the freq_band string name to a unique index. Needed for serialization to HDF5"""
    name_to_idx = FREQ_BANDS['freq_idx']
    assert name_to_idx.index.is_unique
    return power.replace_dim('freq_band', 'freq_band_idx', name_to_idx.reindex(power.coords['freq_band']).values)


def band_idx_to_name(power: Stack):
    """convert the freq_band string name to a unique index. Needed for serialization to HDF5"""
    idx_to_name = pd.Series(FREQ_BANDS.index, FREQ_BANDS['freq_idx'].values)
    assert idx_to_name.index.is_unique
    return power.replace_dim('freq_band_idx', 'freq_band', idx_to_name.reindex(power.coords['freq_band_idx']).values)


def nyquist_freq(sampling_freq: float):
    return sampling_freq / 2


####################################################################################
# Split slow-wave vs REM sleep

def _default_welch_ms(welch_ms, bands: pd.DataFrame):
    """
    Determine a reasonable default value for welch sliding window.

    To determine the Welch sliding window, we take it to include
    at least two full cycles of the lowest frequency of interest.
    For example if the lowest frequency of interest is 0.5 Hz,
    we'll choose: 2 / 0.5 = 4 seconds

    :param welch_ms:
    :param bands:
    :return:
    """
    if welch_ms is None:
        welch_ms = S_TO_MS * 2 / bands.loc[bands['freq_min'] > 0, 'freq_min'].min()

    return welch_ms


def extract_power(traces: stacks.Stack, bands=FREQ_BANDS, welch_ms=None, add_total=True):
    """
    Extract the spectral power for each time trace

    :param traces:
    :param bands:
    :param welch_ms: Welch window. By default twice the period of the lowest freq requested.
    :param add_total: wether to total power to the bands index
    :return:
    """
    assert traces.dims[-1] == 'time'
    sampling_rate = traces.estimate_sampling_rate()
    axis = -1

    valid = bands['freq_min'] < (sampling_rate * .5)
    if not valid.all():
        logging.warning(
            f'Bands too high to extract: {bands.index[~valid]} '
            f'Must be below nyquist_freq {sampling_rate * .5})')

    welch_ms = _default_welch_ms(welch_ms, bands)

    shape = traces.values.shape

    sample_frequences, power_spectral_density = scipy.signal.welch(
        traces.values.reshape(-1, shape[-1]),
        axis=axis,
        fs=sampling_rate,
        nperseg=int(welch_ms * MS_TO_S * sampling_rate),
    )

    frequency_resolution = np.diff(sample_frequences)
    assert np.allclose(frequency_resolution, frequency_resolution[0])
    frequency_resolution = frequency_resolution[0]

    # compute the absolute power by approximating the area under the curve
    # using the composite Simpson’s rule
    values = np.empty((len(bands) + (1 if add_total else 0),
                       power_spectral_density.shape[0]), dtype=np.float_)

    for i, (name, freq_range) in enumerate(bands[['freq_min', 'freq_max']].iterrows()):
        mask = (freq_range[0] <= sample_frequences) & (sample_frequences <= freq_range[1])

        # absolute power in uV^2
        values[i] = scipy.integrate.simps(power_spectral_density[:, mask], dx=frequency_resolution, axis=axis)

    # total power
    if add_total:
        values[-1] = scipy.integrate.simps(power_spectral_density, dx=frequency_resolution, axis=axis)

    # build a stack
    values = values.reshape((-1,) + shape[:-1])

    # prepend freq_band and drop time dimensions
    bands = list(bands.index.values)
    if add_total:
        bands = bands + ['total']
    coords = {
        'freq_band': bands,
    }
    # reset_coords will remove non-dimensional coordinates, which we don't want to carry over
    for k, vs in traces.data.reset_coords(drop=True).coords.items():
        if k != 'time':
            coords[k] = vs.values

    return stacks.Stack.from_array(values, coords)


def extract_power_sliding(
        main: Stack,
        sliding_step_ms=1_000.,
        sliding_win_len_ms=20_000.,
        bands=FREQ_BANDS,
        welch_ms=None,
        show_pbar=False,
        add_total=True,
):
    """
    Extract the spectral power for each time trace using a sliding window.
    Note the windows will overlap and the value generated is assigned to its center.
    This means we cannot cover the beginning and end of the trace.

    :param main:
    :param sliding_step_ms:
        how much the sliding window is shifted on each step, 
        it should be aligned with the sampling rate of the signal
    :param sliding_win_len_ms:
        size of the sliding window

    :param bands: list of names of bands to extract, or full df describing them
    :param add_total:
    :param welch_ms:
    :return:
    """
    assert main.get_rel_win().length >= sliding_win_len_ms, \
        f'Data shorter than sliding window ({main.get_rel_win().length} vs {sliding_win_len_ms})'

    if isinstance(bands, list):
        assert all(isinstance(b, str) for b in bands)
        bands = FREQ_BANDS.loc[bands]

    welch_ms = _default_welch_ms(welch_ms, bands)
    assert sliding_win_len_ms > welch_ms, \
        f'Sliding window ({sliding_win_len_ms} ms) must be bigger than ' \
        f'Welch window ({welch_ms}ms; lowest freq: {bands.freq_min.replace(0, np.nan).min()} Hz)'

    extract_power_kwargs = dict(bands=bands, welch_ms=welch_ms, add_total=add_total)

    sliding_wins = timeslice.Windows.build_sliding_on_stack(
        main,
        length_ms=sliding_win_len_ms,
        step_ms=sliding_step_ms,
    )
    sliding_wins = sliding_wins.wins

    sliding_steps = sliding_wins.index
    if show_pbar:
        sliding_steps = pbar(sliding_steps, desc='sliding win')

    start = sliding_wins['start']
    stop = sliding_wins['stop']

    all_powers = {}
    for i in sliding_steps:
        section = main.isel(time=slice(start[i], stop[i]))
        ref_ms = sliding_wins.loc[i, 'ref_ms']
        all_powers[ref_ms] = extract_power(section, **extract_power_kwargs)

    all_powers = stacks.stackup(all_powers, 'time')

    # put time first
    all_powers = all_powers.transpose(all_powers.dims[-1], ..., all_powers.dims[0])

    return all_powers


def assign_cat_by_hysteresis(values: np.ndarray, cat_defs: pd.DataFrame, other='unknown'):
    """
    we're using hysteresis to define our states in the sorted time series

    We activate a category if we are in no state and match the start condition.
    We deactivate it if we match the stop condition.
    Each condition for each category (start or stop), specifies a range of values [start_vmin, start_vmax]
    and a minimum number of contigous steps [start_cmin] in the condition.

    :param other:

    :param values:
        time series to categorise

    :param cat_defs:
        example:

             stop_vmin  stop_vmax  stop_cmin  start_vmin  start_vmax  start_cmin
        sws       -inf        0.0          0         0.8         inf           0
        rem       -0.5        inf          0        -inf        -0.8           0

    :return:
        an array of the index of cat_defs
        values that could not be categorised will be assigned "other"

    """

    cats = _assign_cat_by_hysteresis_jit(
        values,
        cat_defs[['stop_vmin', 'stop_vmax', 'stop_cmin']].values,
        cat_defs[['start_vmin', 'start_vmax', 'start_cmin']].values,
    )

    names = pd.Series(cat_defs.index)

    return names.reindex(cats, fill_value=other).values


@numba.njit
def _assign_cat_by_hysteresis_jit(values: np.ndarray, cat_stop: np.ndarray, cat_start: np.ndarray):
    """see assign_cat_by_hysteresis"""

    assert cat_start.shape == cat_stop.shape

    cats = np.ones(len(values), dtype=np.int_) * -1

    active_state = -1
    count_in_state = 0
    count_wanting_change = np.zeros(len(cat_start))

    for i, v in enumerate(values):
        if active_state != -1:

            vrange = cat_stop[active_state]

            if (vrange[0] <= v) and (v <= vrange[1]):
                count_wanting_change[active_state] = count_wanting_change[active_state] + 1

                if vrange[2] <= count_wanting_change[active_state]:
                    active_state = -1
                    count_in_state = 0
                    count_wanting_change = np.zeros(len(cat_start))

        if active_state == -1:
            for j in range(len(cat_start)):

                vrange = cat_start[j]

                if (vrange[0] <= v) and (v <= vrange[1]):
                    count_wanting_change[j] = count_wanting_change[j] + 1

                    if vrange[2] <= count_wanting_change[j]:
                        active_state = j
                        count_in_state = 0
                        count_wanting_change = np.zeros(len(cat_start))
                        break

        cats[i] = active_state
        count_in_state = count_in_state + 1

    return cats


####################################################################################
# Detect Sharp-Waves


def find_sharp_waves(
        raw_trace: Stack,
        low_pass_hz=30,
        downsample_hz=10,
        width_ms=500.,
        height=2.,
        prominence=1,
):
    """
    Given a trace in memory, detect sharp-waves in it

    Find SW times using a band pass and find peaks

    :return:
    """
    trace = raw_trace.low_pass(low_pass_hz)
    trace = trace.downsample(downsample_hz)
    trace = trace.zscore()

    peaks = trace.find_peaks(
        width_ms=width_ms,
        height=height,
        prominence=prominence,
        negative=True
    )

    return peaks
