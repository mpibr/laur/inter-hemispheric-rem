"""
Code to handle samples, events and collections of events
"""

import matplotlib.colors
import numba
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm.auto import tqdm as pbar

from ihrem import plot as splot
from ihrem.events import Events


@numba.njit(parallel=True)
def _extract_cdf_other_nb(this, null):
    counts = np.empty(this.shape[0])
    for i in numba.prange(this.shape[0]):

        # noinspection PyTypeChecker
        mask: np.ndarray = (null >= this[i])

        which = np.ones(mask.shape[0], dtype=np.bool_)

        for row in mask.T:
            which = which & row

        count = np.count_nonzero(which)
        counts[i] = count

    return counts / null.shape[0]


class SharpNegativeEvents(Events):
    def __init__(self, reg: pd.DataFrame):
        super().__init__(reg)
        self.reg.rename_axis(index='event_id', inplace=True)

    @staticmethod
    def _grow_peaks(stop_zone: np.ndarray, ref_idcs: np.ndarray) -> (np.ndarray, np.ndarray):
        """
        Given some points in time, grow them in either direction until we reach a given threshold
        """
        start_idcs = np.zeros(len(ref_idcs), dtype=np.int64)
        stop_idcs = np.ones(len(ref_idcs), dtype=np.int64) * len(stop_zone)

        for i in pbar(range(len(ref_idcs)), desc='peaks'):
            ref_idx = ref_idcs[i]
            start_idcs[i] = ref_idx - np.argmax(stop_zone[:ref_idx][::-1])
            stop_idcs[i] = ref_idx + np.argmax(stop_zone[ref_idx:]) - 1

        return start_idcs, stop_idcs

    @classmethod
    def from_double_acceleration(
            cls,
            main,
            band_hz=(.25, 40),
    ):
        assert len(main.dims) == 1

        filtered = main.filter_pass(band_hz)

        speed = filtered.gradient()
        acc = speed.gradient()

        neg_spe = speed.find_peaks(negative=True, height=0, prominence=0, width=0)
        neg_acc = acc.find_peaks(negative=True, height=0, prominence=0, width=0)
        pos_acc = acc.find_peaks(negative=False, height=0, prominence=0, width=0)

        all_peaks = pd.concat({
            'start': neg_acc,
            'ref': neg_spe,
            'stop': pos_acc,
        },
            axis=0, names=['point']
        )
        all_peaks.sort_values('time', inplace=True)
        all_peaks.reset_index('point', inplace=True)
        all_peaks.reset_index(drop=True, inplace=True)

        all_peaks.rename(
            columns=dict(
                sample_idx='idx',
                width_height='rel_height',
            ),
            inplace=True,
        )

        consecutive = (
                (all_peaks['point'].values[:-2] == 'start') &
                (all_peaks['point'].values[1:-1] == 'ref') &
                (all_peaks['point'].values[2:] == 'stop')
        )
        start = all_peaks.index[:-2][consecutive]
        ref = all_peaks.index[1:-1][consecutive]
        stop = all_peaks.index[2:][consecutive]

        assert len(start) == len(stop) and len(start) == len(ref)

        cols = ['time', 'idx', 'prominence', 'rel_height']

        events = pd.concat([
            all_peaks.loc[start, cols].add_prefix('start_').reset_index(drop=True),
            all_peaks.loc[ref, cols].add_prefix('ref_').reset_index(drop=True),
            all_peaks.loc[stop, cols].add_prefix('stop_').reset_index(drop=True),
        ],
            axis=1,
        )

        sns = cls(events)

        sns = sns.lookup_values('speed', speed.values)
        sns = sns.lookup_values('acc', acc.values)
        sns = sns.lookup_values('raw', main.values)
        sns = sns.lookup_values('filt', filtered.values)

        sns.reg['acc_diff'] = sns.reg['stop_acc'] - sns.reg['start_acc']
        sns.reg['amplitude_filt'] = sns.reg['stop_filt'] - sns.reg['start_filt']
        sns.reg['amplitude'] = sns.reg['stop_raw'] - sns.reg['start_raw']
        sns.reg['duration'] = sns.reg['stop_time'] - sns.reg['start_time']
        sns.reg['speed'] = sns.reg['amplitude'] / sns.reg['duration']

        return sns

    @classmethod
    def from_negative_deflections(
            cls, main,
            ref_band_hz=(.25, 100),
            peak_band_hz=(.25, 40),
            low_speed=-.01, width_ms=2,
            height=.01, prominence=.01,
            negative=True,
    ):
        """
        Detect Sharp Negative events by detecting the time of a peak
        of negative derivative and build the smallest window that contains it,
        with edges in a low derivative regime.
        """

        main_ref = main.band_pass(*ref_band_hz)
        speed_ref = main_ref.diff('time')

        speed_smooth = main.band_pass(*peak_band_hz).diff('time')

        all_events = []

        for sel, _ in main.iter_except('time'):
            speed_trace_smooth = speed_smooth.sel(**sel)
            peaks = speed_trace_smooth.find_peaks(
                width_ms=width_ms,
                height=height,
                prominence=prominence,
                negative=negative,
            )

            speed_trace_raw = speed_ref.sel(**sel)
            start, stop = cls._grow_peaks((speed_trace_raw > low_speed).values, peaks['sample_idx'].values)

            events = pd.DataFrame({
                'prominence': peaks['prominence'],
                'start_idx': start,
                'stop_idx': stop,
                'ref_idx': peaks['sample_idx'],
                **sel
            })

            keep_cols = [
                'prominence',
                # 'peak_height',
                # 'width',
                # 'width_height',
            ]
            for col in keep_cols:
                if col in peaks.columns:
                    events[col] = peaks[col]

            all_events.append(events)

        all_events = cls(pd.concat(all_events, axis=0, ignore_index=True, sort=False))

        all_events = all_events.lookup_values('time', main_ref.coords['time'])
        all_events = all_events.lookup_values_per_channel('value', main_ref)
        all_events = all_events.lookup_values_per_channel('speed', speed_ref)

        all_events.reg['amplitude'] = all_events.reg['stop_value'] - all_events.reg['start_value']
        all_events.reg['duration'] = all_events.reg['stop_time'] - all_events.reg['start_time']
        all_events.reg['speed'] = all_events.reg['amplitude'] / all_events.reg['duration']

        return all_events

    def plot_inter_event_interval_hists(self, col='ref', step=100, facecolor='k', figsize=(3, 3)):
        iei = self.get_inter_event_intervals(first=col, second=col)

        f, axs = plt.subplots(constrained_layout=True, nrows=2, figsize=figsize)

        ax = axs[0]

        ax.hist(
            iei,
            np.arange(0, iei.max() + step, step),
            facecolor=facecolor,
        )

        ax.set(
            title='inter-event interval',
            xlabel='ms',
            ylabel='count',
        )

        ax.spines['bottom'].set_position(('outward', 2))

        ax = axs[1]

        ax.hist(
            iei,
            np.geomspace(1, iei.max(), 101),
            facecolor=facecolor,
        )

        ax.set(
            title='inter-event interval (log)',
            xlabel='ms',
            ylabel='count',
        )

        ax.spines['bottom'].set_position(('outward', 2))
        ax.set_xscale('log')

    def _get_bins(self, col, default=200):
        bins = default

        if col == 'duration':
            step = self.reg[col].sort_values().diff().replace(0, np.nan).min()
            bins = np.arange(-.5 * step, self.reg[col].max() + 1.5 * step, step)

        if isinstance(bins, int):
            bins = np.linspace(
                self.reg[col].min(),
                self.reg[col].max(),
                default + 1
            )

        return bins

    def plot_props(self, cols=('amplitude', 'duration', 'speed'), figsize=(6, 1.5), default_bins=200):
        cols = list(cols)

        f, axs = plt.subplots(constrained_layout=True, figsize=figsize, ncols=len(cols), squeeze=False)

        for j, col in enumerate(cols):
            ax = axs.ravel()[j]

            bins = self._get_bins(col, default=default_bins)

            ax.hist(
                self.reg[col],
                bins=bins,
                color='k',
            )
            ax.set(xlabel=col.replace('_', ' '), ylabel='#SN')
            ax.spines['bottom'].set_position(('outward', 2))

        return axs

    def plot_props_hist2d(
            self,
            xcol='duration', ycol='amplitude',
            desc_loc='upper right',
            ax=None, bins=(100, 100), norm='log',
            cmap='cividis',
            show_cbar=True,
    ):
        if ax is None:
            f, ax = plt.subplots(constrained_layout=True, figsize=(3, 2))

        if norm == 'log':
            norm = matplotlib.colors.LogNorm()

        h, xedges, yedges, im = ax.hist2d(
            self.reg[xcol],
            self.reg[ycol],
            bins=bins,
            norm=norm,
            cmap=cmap,
        )
        ax.set(
            xlabel=xcol.replace('_', ' '),
            ylabel=ycol.replace('_', ' '),
        )

        if show_cbar:
            ax.figure.colorbar(im, aspect=100, ax=ax).set_label('#SN')

        if desc_loc is not None:
            splot.add_desc(ax, f'n={len(self.reg):,g}', loc=desc_loc)

        return ax

    def plot_props_hist2d_matrix(
            self, cols=('amplitude_log', 'duration', 'speed'),
            bin_count=200,
            figsize=(8, 5),
            constrained_layout=True,
            highlight_groups=None,
            highlight_colors=None,
    ):

        if highlight_groups is None:
            highlight_groups = []

        if highlight_colors is None:
            highlight_colors = [f'C{i}' for i, _ in enumerate(highlight_groups)]

        f, axs = plt.subplots(
            figsize=figsize, constrained_layout=constrained_layout,
            ncols=len(cols), nrows=len(cols),
            sharex='col',
        )

        for i in range(axs.shape[0]):
            for j in range(axs.shape[1]):
                ax = axs[i, j]

                if i < j:
                    ax.axis('off')

                else:

                    df = {}
                    for col in cols[i], cols[j]:

                        if col not in self.reg.columns and col.endswith('_log'):

                            series = self.reg[col[:-len('_log')]]

                            series = np.log10(series.replace(0, np.nan).abs())

                        else:
                            series = self.reg[col]

                        df[col] = series

                    df = pd.DataFrame(df).dropna()

                    if i == j:
                        ax.hist(
                            df.iloc[:, 0],
                            bins=bin_count,
                            color='k',
                        )
                        if i != 0:
                            ax.spines['left'].set_visible(False)
                            ax.spines['right'].set_visible(True)
                            ax.yaxis.tick_right()
                            ax.yaxis.set_label_position('right')
                        ax.set_ylabel('count')

                    else:

                        ax.hist2d(
                            df.iloc[:, 1],
                            df.iloc[:, 0],
                            norm=matplotlib.colors.LogNorm(),
                            bins=(bin_count, bin_count),
                        )
                        if j != 0:
                            ax.tick_params(left=False, labelleft=False)
                            ax.spines['left'].set_visible(False)

                        if i < axs.shape[0] - 1:
                            ax.tick_params(bottom=False, labelbottom=False)
                            ax.spines['bottom'].set_visible(False)

                        for k, group in enumerate(highlight_groups):
                            # we had dropped nans
                            group = group.intersection(df.index)

                            ax.scatter(
                                df.loc[group].iloc[:, 1],
                                df.loc[group].iloc[:, 0],
                                s=5,
                                edgecolor='k',
                                facecolor=highlight_colors[k],
                                marker='.',
                                linewidth=.125,
                            )

        for j, ax in enumerate(axs[-1, :]):
            ax.set_xlabel(cols[j].replace('_', ' '), fontsize=6)

        for i, ax in enumerate(axs[:, 0]):
            if i != 0:
                ax.set_ylabel(cols[i].replace('_', ' '), fontsize=6)

        return axs

    def plot_props_contour_by(
            self, ax,
            xcol='duration', ycol='amplitude', by='is_valid', cmaps=None,
            bins=(100, 100), linewidths=.5, levels=5,
    ):

        if cmaps is None:
            cmaps = {True: 'Oranges', False: 'Greens'}

        for by_val, by_sel in self.reg.groupby(by):
            h, xedges, yedges = np.histogram2d(
                by_sel[xcol],
                by_sel[ycol],
                bins=bins,
            )

            # noinspection PyTypeChecker
            ax.contour(
                h.T,
                extent=(
                    xedges[0], xedges[-1],
                    yedges[0], yedges[-1],
                ),
                levels=levels,
                linewidths=linewidths,
                cmap=cmaps[by_val],
            )

        ax.set(
            xlabel=xcol.replace('_', ' '),
            ylabel=ycol.replace('_', ' '),
        )

    def plot_props_hist_by(
            self, ax, col='duration', by='is_valid',
            bins=100, facecolors=None, alpha=.5, col_label=None, count_label='count',
            orientation='vertical',
            **kwargs
    ):

        if facecolors is None:
            facecolors = {True: 'xkcd:orange', False: 'xkcd:green'}

        by_desc = by.replace('_', ' ')

        for by_val, by_sel in self.reg.groupby(by):

            if not isinstance(by_val, bool):
                by_val_desc = f'{by_desc}={by_val}'
            else:
                if by_desc.startswith('is '):
                    by_desc = by_desc[3:]

                if by_val:
                    by_val_desc = by_desc

                else:
                    by_val_desc = 'not ' + by_desc

            ax.hist(
                by_sel[col],
                bins=bins,
                facecolor=facecolors[by_val],
                alpha=alpha,
                orientation=orientation,
                label=f'{len(by_sel):,d} {by_val_desc}',
                **kwargs,
            )

        if orientation == 'vertical':
            ax.spines['bottom'].set_position(('outward', 2))
            ax.set(
                xlabel=col.replace('_', ' ') if col_label is None else col_label,
                ylabel='count' if count_label is None else count_label,
            )

        else:
            ax.spines['left'].set_position(('outward', 2))
            ax.set(
                ylabel=col.replace('_', ' ') if col_label is None else col_label,
                xlabel='count' if count_label is None else count_label,
            )

    def plot_props_hist(
            self, ax, col='duration',
            bins=100, facecolor='k', col_label=None, count_label='count',
            orientation='vertical',
            **kwargs
    ):

        ax.hist(
            self[col].dropna(),
            bins=bins,
            orientation=orientation,
            facecolor=facecolor,
            **kwargs,
        )

        if orientation == 'vertical':
            ax.spines['bottom'].set_position(('outward', 2))
            ax.set(
                xlabel=col if col_label is None else col_label,
                ylabel='count' if count_label is None else count_label,
            )

        else:
            ax.spines['left'].set_position(('outward', 2))
            ax.set(
                ylabel=col if col_label is None else col_label,
                xlabel='count' if count_label is None else count_label,
            )

    def plot_props_contour_by_margin(
            self, xcol='amplitude', ycol='duration', by='is_valid', suptitle='', xbins=100, ybins=100,
            figsize=(4, 3),
            show_legend=True,
            **kwargs,
    ):

        axs = splot.get_ax_with_marginals(figsize=figsize)

        axs['main'].figure.suptitle(suptitle)

        self.plot_props_contour_by(
            axs['main'],
            xcol=xcol, ycol=ycol, by=by,
            bins=(xbins, ybins),
            **kwargs,
        )
        self.plot_props_hist_by(
            axs['xmargin'],
            col=xcol, by=by,
            col_label='',
            bins=xbins,
        )
        self.plot_props_hist_by(
            axs['ymargin'],
            col=ycol, by=by,
            col_label='', orientation='horizontal',
            bins=ybins,
        )

        if show_legend:
            axs['xmargin'].legend(loc='upper left', fontsize=6)

        return axs

    def plot_props_hist2d_margin(
            self, xcol='amplitude', ycol='duration', suptitle='', xbins=100, ybins=100,
            figsize=(4, 3),
            cmap='cividis',
            xfacecolor='k',
            yfacecolor='k',
            desc_loc='upper right',
            norm=None,
            show_cbar=False,
    ):

        axs = splot.get_ax_with_marginals(figsize=figsize)

        axs['main'].figure.suptitle(suptitle)

        self.plot_props_hist2d(
            ax=axs['main'],
            xcol=xcol, ycol=ycol,
            bins=(xbins, ybins),
            norm=norm,
            cmap=cmap,
            desc_loc=desc_loc,
            show_cbar=show_cbar,
        )

        self.plot_props_hist(
            ax=axs['xmargin'],
            bins=xbins,
            col=xcol,
            col_label='',
            facecolor=xfacecolor,
        )

        self.plot_props_hist(
            ax=axs['ymargin'],
            bins=ybins,
            col=ycol, orientation='horizontal',
            col_label='',
            facecolor=yfacecolor,
        )

        return axs

    def plot_vs_time(self, ax, x='time', y='raw', **kwargs):
        kwargs.setdefault('linewidth', 1)
        kwargs.setdefault('color', 'k')

        for i in pbar(self.reg.index):
            ax.plot(
                self.reg.loc[i, [f'start_{x}', f'ref_{x}', f'stop_{x}']].values,
                self.reg.loc[i, [f'start_{y}', f'ref_{y}', f'stop_{y}']].values,
                **kwargs,
            )

    def extract_cdf_other(self, sns_null, cols=('start_acc', 'stop_acc', 'ref_speed')):

        cols = list(cols)

        rates = _extract_cdf_other_nb(
            self.reg[cols].abs().values,
            sns_null.reg[cols].abs().values,
        )

        return pd.Series(rates, self.reg.index)

    def add_details(self, all_beta):
        beta_max = all_beta.max(axis=1)
        copy = self.lookup_values_interp(f'beta_max', beta_max, cols=['ref'])

        for ch, beta in all_beta.items():
            copy = copy.lookup_values_interp(f'beta_ch{ch}', beta, cols=['ref'])

        copy.reg['ref_beta_local'] = pd.concat([
            copy.sel(channel=ch).reg[f'ref_beta_ch{ch}']
            for ch in all_beta.columns

        ]).sort_index()

        copy.reg['duration_log'] = np.log10(np.abs(copy.reg.loc[copy.reg['duration'] > 0, 'duration']))
        copy.reg['amplitude_log'] = np.log10(np.abs(copy.reg.loc[copy.reg['amplitude'] < 0, 'amplitude']))
        copy.reg['amplitude_filt_log'] = np.log10(np.abs(
            copy.reg.loc[copy.reg['amplitude_filt'] < 0, 'amplitude_filt']
        ))
        copy.reg['speed_log'] = np.log10(np.abs(copy.reg.loc[copy.reg['speed'] < 0, 'speed']))

        copy.reg['ref_speed_log'] = np.log10(np.abs(copy.reg.loc[copy.reg['ref_speed'] < 0, 'ref_speed']))
        copy.reg['start_acc_log'] = np.log10(np.abs(copy.reg.loc[copy.reg['start_acc'] < 0, 'start_acc']))
        copy.reg['stop_acc_log'] = np.log10(np.abs(copy.reg.loc[copy.reg['stop_acc'] > 0, 'stop_acc']))
        copy.reg['acc_diff'] = copy.reg['stop_acc'] - copy.reg['start_acc']
        copy.reg['speed_diff'] = copy.reg['stop_speed'] - copy.reg['start_speed']

        copy.reg['ref_to_stop_time'] = copy.reg['stop_time'] - copy.reg['ref_time']
        copy.reg['ref_to_start_time'] = copy.reg['start_time'] - copy.reg['ref_time']

        for col in ['acc_diff', 'ref_prominence', 'start_prominence', 'stop_prominence']:
            copy.reg[f'{col}_log'] = np.log10(np.abs(copy.reg.loc[copy.reg[col] > 0, col]))

        return copy

    def add_isi(self):
        copy = self.reg.copy()
        copy['isi_next'] = self.get_inter_event_intervals()
        copy['isi_prev'] = copy['isi_next'].shift(1)
        return self.__class__(copy)

    def assign_matches(
            self,
            path: pd.DataFrame,
            name='match',
            cols=(
                    'amplitude', 'speed', 'duration',
                    'ref_time', 'start_time',
            ),
            quiet=False
    ):
        copy = self.reg.copy()

        copy.loc[path[0], name] = path[1].values
        copy.loc[path[1], name] = path[0].values

        matches = copy[name].dropna().astype(int)

        for col in cols:
            copy.loc[matches.index, f'{name}_{col}'] = copy.loc[matches.values, col].values
            copy[f'{name}_{col}_diff'] = copy[f'{name}_{col}'] - copy[col]

        if not quiet:
            matched_count = np.count_nonzero(copy[name].notna())
            print(f'matched {matched_count:,g}/{len(copy):,g} ({100 * matched_count / len(copy):.1f}%) events')

        return self.__class__(copy)

    def get_labels_gaussian_mix(self, key_cols):
        valid = self.reg[key_cols].dropna()

        import sklearn.mixture
        mixture = sklearn.mixture.GaussianMixture(n_components=2, covariance_type='spherical').fit(valid)
        labels = pd.Series(mixture.predict(valid), valid.index)

        sorted_labels = self.reg.groupby(labels)['amplitude'].mean().abs().sort_values().index

        labels = labels.map(dict(zip(sorted_labels, np.arange(len(sorted_labels)))))

        return labels

    def get_labels_multichan(self, key_cols):
        all_labels: pd.Series = pd.concat([
            self.sel(channel=ch).get_labels_gaussian_mix(key_cols)
            for ch in self.reg['channel'].unique()
        ])

        # noinspection PyUnresolvedReferences
        assert all_labels.index.is_unique
        return all_labels.reindex(self.reg.index)

    def get_match_diff(self, col, match_col=None, name='match') -> pd.Series:

        if match_col is None:
            match_col = col

        this = self.reg[col]
        that = self.get_match(match_col=match_col, name=name)

        return this - that

    def get_match(self, match_col, name='match') -> pd.Series:

        matches = self.reg[name]

        missing = ~matches.dropna().isin(self.reg.index)

        if np.any(missing):
            print(f'Missing data for {np.count_nonzero(missing)}/{len(missing)} match ids')

        values = self.reg[match_col].reindex(matches.values).values

        return pd.Series(values, index=matches.index)

    def get_pairs(self, ref_ch=0, cols=('ref_time',), name='match') -> pd.DataFrame:
        cols = list(cols)

        matched = self.sel_mask(self[name].notna())

        ch0 = matched.sel(channel=ref_ch)[cols + [name]].reset_index()

        ch0_cols = ch0.drop(name, axis=1).add_prefix(f'ch{ref_ch}_')

        match_ids = ch0[name].values

        ch1 = self.reg.loc[match_ids][cols].reset_index()
        ch1['event_id'] = ch1['event_id'].astype(int)

        other_ch = [1, 0][ref_ch]
        ch1_cols = ch1.add_prefix(f'ch{other_ch}_')

        return pd.concat([ch0_cols, ch1_cols], axis=1)

    def _get_simplified_channel_idx(self):
        mapping = self.reg[['probe', 'channel']].drop_duplicates().sort_values(['probe', 'channel']).copy()

        mapping_code = mapping['probe'] * 1e6 + mapping['channel']

        mapping_code = pd.Series(np.arange(len(mapping)), index=mapping_code.values)

        code = self.reg['probe'] * 1e6 + self.reg['channel']

        return code.map(mapping_code)

    def patch_simplified_channels(self):
        """
        Replace the true channel id with a consecutive ch0, ch1, ch2, etc.

        This is useful to handle different experiments where we use different channels
        due to SNR or position, but they all represent the same place.

        New channels are assigned consecutive integer values based on their
        sorted probe+channel id.

        :return: modified copy
        """

        new = self.reg.copy()
        new['channel_true'] = new['channel']
        new['channel'] = self._get_simplified_channel_idx()

        return self.__class__(new)

    @classmethod
    def load_matched_sns(cls, reg, exp_name, sn_suffix='_cdf', matching_suffix='_lax', drop_singles=False):
        """
        Return all detected SNs with bilateral matching.
        If not dropped, single (unmatched) SNs are included with nan in "match" column.
        """
        sns = cls(reg.load_all_sne(exp_name, suffix=sn_suffix))
        sns = sns.patch_simplified_channels()
        assert sns['channel'].isin([0, 1]).all()

        # noinspection PyTypeChecker
        matching: pd.DataFrame = pd.read_hdf(reg.get_path_matching(exp_name, suffix=matching_suffix))
        matched = sns.assign_matches(matching)
        matched['is_lead'] = matched['match_ref_time_diff'] > 0

        if drop_singles:
            matched = matched.sel_mask(matched['match'].notna())

        return matched

    def is_match_valid(self, match_col='match'):
        """
        Boolean mask indicating for each SN if they have a valid match.

        :param match_col:
        :return:
        """
        matches = self.reg[match_col]
        return matches.isin(self.reg.index)

    def drop_missing_matches(self, match_col='match'):
        """
        Set the match column to nan if the match is not in the registry.
        This happens sometimes when matching was done for a full experiment
        but we are looking at a slice. In that case the match of an SN may
        foll outside the slice, so we want to treat it as a single.
        """
        valid = self.is_match_valid(match_col=match_col)
        new = self.reg.copy()
        new.loc[~valid, match_col] = np.nan
        return self.__class__(new)

    def get_rate_by_match(self, rate_bin_ms=1000, rate_rolling_ms=5000):
        matched = self.sel(is_lead=True)

        rates = {}

        load_win = (matched['ref_time'].min(), matched['ref_time'].max())

        if rate_rolling_ms is not None:
            rolling_win = int(rate_rolling_ms / rate_bin_ms)
        else:
            rolling_win = None

        for ch in [0, 1]:
            by_ch = matched.sel(channel=ch)
            rates[ch] = by_ch.get_rate(load_win, step=rate_bin_ms, rolling_win=rolling_win).dropna()

        return pd.DataFrame(rates).rename_axis(index='time', columns='channel')
